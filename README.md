# The_GIS Project



## Prerequisite libraries

To launch the project, it is necessary to have installed:

```
sudo apt-get update
sudo apt-get install build-essential cmake libgtest-dev libglfw3-dev libglew-dev qt5-default libgdal-dev libglm-dev
```


## Docker

### Tests

If you need to test the application, you can do it by typing in a terminal in your repository directory:
```
./Start_tests
```
It will install all libraries needed in a ubuntu docker image, compile your code and launch the tests in an ubuntu docker container.

### Software

If you need to launch the application, you can do it by typing:
```
./Start_TheGIS
```
It will install all libraries needed in a ubuntu docker image, compile your code and launch it in an ubuntu docker container.

### Add Lib

If you need to use a new library for a fonctionnality you are implementing, you need to add its installation to the Dockerfile as well. This is important since Docker is the platform we will use to deliver our code after each sprint.

## Developper documentation

Developper documentation can be found in MonSIG3D/html and MonSIG3D/latex. It will provide you a summary of each class and method implemented in our GIS.

