/**
 * @file Controller.h
 * @author TSI
 * @brief 
 * @version 0.1
 * @date 2021-11-18
 * 
 * @copyright Copyright (c) 2021
 * 
 * Modifs : Function getVertexToPaint
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * Modifs : Add display3D attribute and functions to handle the change of dimension
 * Claire-Marie Alla, Maxime Charzat - 25/11/2021
 * Modifs : add getField method
 * Adrienne Zebaze, Claire-Marie Alla - 2021-11-29
 * Modifs : add AddFieldHauteur method
 * Claire-Marie Alla - 2021-11-29
 * Modifs : Function mousePressEvent
 * Charles Laverdure, Théo Huard, Maxime Charzat   - 29-11-24
 * Modifs : add the possibility to change the mntPath during loading a geotiff
 * Maxime Charzat - 2021-12-01
 */

#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <Data.h>
#include <iostream>
#include <Model.h>
#include <vector>
#include <Model.h>
#include <string>

using namespace std;

class Controller
{
private:
    /**
     * @brief model pointer
     */
    Model * model;
    /**
     * @brief display3D
     */
    bool display3D = false;

public:
    /**
     * @brief Controller
     * Constructor for Controller
     *
     * @param model
     */
    Controller(Model * model);
    /**
     * @brief Destructor for Controller
     *
     */
    ~Controller();
    /**
     * @brief load
     * load a file of unknown type
     *
     * @param path
     */
    void load(string path);
    /**
     * @brief loadTif
     * load a geotiff file
     *
     * @param path
     * @param name
     *
     * @return int corresponding to loading status
     */
    int loadTif(string path, string name, string mnt_path = "../MonSIG3D/data/GeoTIFF/Lyon_arr5/mnt_arr5.tif");
    /**
     * @brief loadShp
     * load a shapefile
     *
     * @param path
     * @param name
     *
     * @return int corresponding to loading status
     */
    int loadShp(string path, string name);
    /**
     * @brief remove
     * remove a layer from the view with its id
     *
     * @param id
     */
    void removeLayer(string name);
    /**
     * @brief update_view
     * update the view when the user move the camera
     *
     * @return
     */

    /**
     * @brief setVisibility
     * update visibility of layer
     * @param name of layer
     * @param new visibility
     */
    void setVisibility(string name, bool visibility);

    /**
     * @brief getLayersToPaint
     * @return
     */
    vector<Data*> getLayersToPaint();

    /**
     * @brief getDisplay3D
     * @return
     */
    bool getDisplay3D() {return display3D;}

    /**
     * @brief changeDisplay3D
     * @return
     */
    void changeDisplay3D() {this->display3D = !display3D;}

    /**
     * @brief setSwithOverXD
     */
    void setSwithOverXD();
    
    /**
     * @brief verify file extent
     *
     */
    bool verifyExtent(string path, string extent);

    /**
     * @brief checkName
     * @param name
     * @return The Final name
     */
    string checkName(string name);

    /**
     * @brief move two item in the model
     * @param name1
     * @param name2
     */
    void moveItem(string name1, string name2);

    /**
     * @brief getFields
     * @param id
     * @return vector of fields
     */
    vector<string> getFields();

    /**
     * @brief addfieldHauteur
     * @param field
     */
    void addFieldHauteur(string field);

    /**
     * @brief getWfsLayersName
     * @param name
     * @param path
     * @return
     */
    vector<string> getWfsLayersName(string name , string path);

    /**
     * @brief generateWfsMetaData
     * @param layerName
     */
    bool generateWfsMetaData(string layerName);

   /**
    * @brief getLayerNameStream
    * @param path
    * @return
    */
   vector<string> getLayerNameStream(string path);

   /**
    * @brief loadVertexLayerStream
    * @param name
    */
   void loadVertexLayerStream(string name);

   /**
    * @brief Controller::loadStream
    * @param path
    * @param name
    * @return
    */
   int loadStream(string path, string name);

    /**
     * @brief get the clicked point
     *
     */
    vector<string> getPointInfo(OGRPoint* clickedPoint);



    //Tests
    friend class controller_loadShpGood_Test;
    friend class controller_loadShpBad_Test;
    friend class controller_loadTifGood_Test;
    friend class controller_loadTifBad_Test;
    friend class controller_verifyExtent_Test;
    friend class controller_getVertex_Test;
    friend class controller_getVertexVisibility_Test;
    friend class controller_addFieldHauteur_Test;
};

#endif
