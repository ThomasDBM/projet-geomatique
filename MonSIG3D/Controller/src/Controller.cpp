/**
 * @file Controller.cpp
 * @author TSI
 * @brief 
 * @version 0.1
 * @date 2021-11-18
 *
 *
 * 
 * @copyright Copyright (c) 2021
 * 
 * Modifs : adding function verifyExtent
 * Miancien Paul - 2021-11-23
 * Modifs : Function getVertexToPaint
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * Modif Theo herman, add implementation functions 24/11/21
 * Modifs : Add display3D attribute and functions to handle the change of dimension
 * Claire-Marie Alla, Maxime Charzat - 25/11/2021
 * Modifs : Implement setVisibility function
 * Rouyer Nicolas - 2021-11-26
 * Modifs : implement getField method
 * Adrienne Zebaze, Claire-Marie Alla - 2021-11-29
 * Modifs : implement AddFieldHauteur method
 * Claire-Marie Alla - 2021-11-29
 * Modifs : Implement loading stream
 * Monge Maïlys -2021-11-29
 * Modifs : add the possibility to change the mntPath during loading a geotiff
 * Maxime Charzat - 2021-12-01
 * Modifs : verify type of the layer for the info retrieval
 * Maxime Charzat - 2021-12-01
 */

#include <Controller.h>
#include <Shapefile.h>
#include <vector>
#include <iostream>
#include <gdal/ogrsf_frmts.h>
#include <gdal/gdal.h>
#include <gdal/cpl_string.h>
#include <iostream>
#include <regex>




Controller::Controller(Model * model){
    this->model = model;
}

Controller::~Controller()
{
}

void Controller::load(string type){

}

int Controller::loadTif(string path,string name, string mntPath){
    return this->model->addLayer("geotiff",name,path,mntPath);
}

int Controller::loadShp(string path, string name){
    return this->model->addLayer("shapefile",name,path);
}

int Controller::loadStream(string path, string name){
    return this->model->addLayer("wfs",name,path);
}


void Controller::removeLayer(string name){

    if(model->getLayerByName(name)->getType()=="Wfs"){
             model->clearVertex(name);
             return;
         }

    model->removeLayer(name);

}

void Controller::setVisibility(string name, bool visibility){
    for (int k = 0; k < model->getNumberOfLayers(); k++){
        Data* layer = model->getLayerByPosition(k);
        if (layer->getName() == name){
            layer->setVisibility(visibility);
        }
    }
}

vector<Data*> Controller::getLayersToPaint(){
    vector<Data*> toPaint;

    for (int k = 0; k < model->getNumberOfLayers(); k++){
        Data* layer = model->getLayerByPosition(k);
        if (layer->getVisibility()){
            toPaint.push_back(layer);
        }
    }
    return toPaint;
}

bool Controller::verifyExtent(string path, string extent) {
    if(path.substr(path.length()-extent.length(), path.length()-1) == extent)
    {
         std::cout << "Good format.\n";
         return true;
    } else {
         std::cout << "Wrong format.\n";
         return false;
    }
}

string Controller::checkName(string name){
    return model->checkName(name);
}

void Controller::moveItem(string name1, string name2){
    model->moveItem(name1,name2);
}

void Controller::setSwithOverXD(){
    changeDisplay3D();

    for (int k = 0; k < model->getNumberOfLayers(); k++){
        Data* layer = model->getLayerByPosition(k);
        if (layer->getExtrude()){
            Shapefile* shape = static_cast<Shapefile*>(layer);
            shape->generateVertex(display3D);
        }
    }
}

vector<string> Controller::getFields(){
    int position = model->getNumberOfLayers()-1;
    return model->getLayerByPosition(position)->getListOfFields();
}

void Controller::addFieldHauteur(string field){

    // Set height field
    int position = model->getNumberOfLayers()-1;
    Data * layer = model->getLayerByPosition(position);

    // Set is extrude
    if (field != "No height"){
        layer->setExtrude(true);
        layer->setField3D(field.c_str());
    }

    Shapefile * shp = static_cast<Shapefile*>(layer);
    shp->generateVertex();
}

vector<string> Controller::getLayerNameStream(string path){
    return model->getLayerStreamByPath(path);
}

void Controller::loadVertexLayerStream(string name){
    model->loadVertexLayerStream(name);
}


vector<string> Controller::getPointInfo(OGRPoint* clickedPoint){

    OGRFeature * feature;
    vector<string>  featureInfos;

    if(getDisplay3D()){
       return vector<string> ();
    }
    else{
    for (int k = 0; k < model->getNumberOfLayers(); k++){


        Data* layer = model->getLayerByPosition(k);
        if (layer->getVisibility() && layer->getType() == "vector"){
            Shapefile* shape = static_cast<Shapefile*>(layer);
            feature = shape->getFeatureFromClick(clickedPoint);
            if(feature != nullptr){
               featureInfos = shape -> getContentFeature(feature);

               return featureInfos;
            }
        }
    }
    return vector<string> ();
    }

}
