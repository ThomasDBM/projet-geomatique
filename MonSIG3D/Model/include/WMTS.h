/* WMTS.h
 * Create the WMTS class
 * Roaa Masri
 * First merge :19/11/21
*/

#ifndef WMTS_H
#define WMTS_H


#include <iostream>
#include<vector>

#include "../include/Data.h"

/**
 * @brief WMTS Class which inherits of Data
 *
 */
class WMTS : public Data
{
private:
    /* data */
public:
    /**
     * @brief Construct a new WMTS object
     *
     * @param name
     * @param path
     * @param vertex
     */
    WMTS(std::string name, std::string path);

    /**
     * @brief Destroy the WMTS object
     *
     */
    ~WMTS();

    /**
     * @brief Calculate the extents of the object
     */
    void calculateExtentsBarycenter() override;


//Tests
    friend class wmts_constructor_Test;
    friend class wmts_destructor_Test;
};


#endif // WMTS_H
