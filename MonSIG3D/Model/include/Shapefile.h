/* Shapefile.h
 * Create the shapefile class who contains all information about a shapefile
 * Claire-Marie Alla, Axelle Gaigé, Maxime Charzat
 * First merge : 10/11/2021
 * Modifs : Add calculate2DLineVertex
 * Modifs : Change type of the vertex
 * Charzat Maxime - 15/11/2021
 * Modifs : Add 2 tests
 * Amaryllis Vignaud - 15/11/2021
 * Modifs : Add calculate2DPolygonVertex
 * Charzat Maxime - 16/11/2021
 * Modifs : Add 4 tests
 * Amaryllis Vignaud - 15 and 16/11/2021
 * Modifs : Refactor functions to combine the generation of 2D and 3D vertex
 * Charzat Maxime - 17/11/2021
 * Modifs : Adapt to archi : add data
 * Axelle Gaige - 19/11/2021
 * Modifs : Vertexes attributes and getters transfered to Data
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * Modifs : Add functions to generate vertex with extrude elements
 * Claire-Marie Alla, Maxime Charzat - 25/11/2021
 * Modifs : Add functions get and set listOfFields for shapefile
 * Amaryllis Vignaud, Axelle Gaige - 2021-11-25
 * Modifs : Add function to triangulate polygons
 * Huard Théo, Maxime Charzat - 26/11/2021
 * Modifs : Add functions to generate and handle vertex for extruded geometries
 * Maxime Charzat, Théo Huard - 2021-11-29
 * Modifs : change generate vertex to have fieldHauteur
 * Claire-Marie Alla - 2021-11-29
*/

#pragma once

#ifndef SHAPEFILE_H
#define SHAPEFILE_H

#include <Data.h>
#include <iostream>
#include <gdal/ogrsf_frmts.h>
#include <glm/glm.hpp>
#include <string>

/**
 * @brief The Shapefile class
 */
class Shapefile : public Data
{
protected:

    int size;

public:

    /**
     * @brief Construct a new Shapefile object
     * @param name
     * @param path
     */
    Shapefile(string name, string path);

    /**
     * @brief Destroy the Shapefile object
     *
     */
    ~Shapefile();

    /**
     * @brief Get size
     * @return char*
     */
    int GetSize(){return size;}

    /**
     * @brief Get Projection of the shapefile
     * @return OGRSpatialReference
     */
    const char* GetProjection();

    /**
     * @brief Reproject Shp if referential is different of 2154
     */
    void ReprojectShp();

    /**
     * @brief Load the shapefile with gdal
     * @return true if succeed and false if not
     */
    bool load();

    /**
     * @brief initializeVertex
     */
    void initializeVertex();

    /**
     * @brief Generate the Vertexes of the shapefile data
     */
    void generateVertex(bool toExtrude = false);

    /**
     * @brief Calculate the Vertex of shapefile data containing points
     * @param geometry of the feature
     */
    void calculatePointVertex(OGRGeometry* geometry);

    /**
     * @brief calculatePointExtrudeVertex
     * @param geometry
     * @param h
     */
    void calculatePointExtrudeVertex(OGRGeometry *geometry, double h);

    /**
     * @brief calculateLineExtrudeVertex
     * @param geometry
     * @param h
     * @param fillLine
     */
    void calculateLineExtrudeVertex(OGRGeometry* geometry, double h, bool fillLine = true);

    /**
     * @brief Calculate the Vertex of a linestring of the shapefile data
     * @param geometry : geometry of the feature
     */
    void calculateLineVertex(OGRGeometry* geometry, bool fillLine = true,
                             bool fillPolygon = false, double h = 0);


    /**
     * @brief Calculate the Vertex of a polygon of the shapefile data
     * @param geometry : geometry of the feature
     */
    void calculatePolygonVertex(OGRGeometry* geometry);

    /**
     * @brief Get all fields of the shapefile
     * @return list of shapefile fields
     */
    vector<string> getFields();

    /**
     * @brief Set all fields of the shapefile
     */
    void setFields();

    /**
     * @brief Calculate the extents of the object
     */
    void calculateExtentsBarycenter() override;
	
    /**
     * @brief calculateTriangulation
     * @param polygon
     * @param i
     * @param nbAttempt
     */
    void calculateTriangulation(OGRPolygon* polygon, double h, unsigned int i = 0, unsigned int nbAttempt = 0);

    /**
     * @brief Get vertex for display with opengl
     * @return std::vector<std::vector<glm::vec3>>
     */
    vector<vector<glm::vec3>> GetVertexTriangulateExtruded(){return vertexTriangulateExtruded;}

    /**
     * @brief Get vertex for display with opengl
     * @return std::vector<std::vector<glm::vec3>>
     */
    vector<vector<glm::vec3>> GetVertexContourExtruded(){return vertexContourExtruded;}

 /**
     * @brief Get all fields of the shapefile
     * @return list of shapefile fields
     */
    vector<string> getContentFeature(OGRFeature* feature);


    /**
     * @brief See if point is inside the geometry
     */
    OGRFeature * getFeatureFromClick(OGRPoint* clickedPoint);



//Tests
    friend class shapefile_constructor_Test;
    friend class shapefile_load_Test;
    friend class shapefile_calculate_vertex_point_size_Test;
    friend class shapefile_calculate_vertex_extrude_point_size_Test;
    friend class shapefile_calculate_vertex_multi_point_size_Test;
    friend class shapefile_calculate_vertex_point_coordinates_Test;
    friend class shapefile_calculate_vertex_multi_coordinates_Test;
    friend class shapefile_calculate_vertex_line_Test;
    friend class shapefile_calculate_vertex_extrude_line_Test;
    friend class shapefile_calculate_vertex_polygon_Test;
    friend class shapefile_calculate_vertex_extrude_polygon_Test;
    friend class shapefile_verif_shp_Test;
    friend class shapefile_verif_dbf_shx_Test;
    friend class shapefile_reprojection_Test;
    friend class shapefile_verif_L93_Test;
    friend class shapefile_verif_WGS84_Test;
    friend class shapefile_verif_getFields_Test;
    friend class shapefile_verif_setFields_Test;
};



#endif // SHAPEFILE_H
