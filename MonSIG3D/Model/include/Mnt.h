#ifndef MNT_H
#define MNT_H

#include <vector>

using namespace std;

class Mnt
{
    private:
    /**
     * @brief Matrice that kept all the value of altitude
     * 
     */
    vector<vector<double>> mnt;

    double x0;
    double y0;
    double rw;
    double rh;
    int w;
    int h;
    int size;

    double min;
    double max;


    public:
    // CONSTRUCTOR
    /**
     * @brief Construct a new Mnt object
     * 
     * @param path 
     */
    Mnt(string path);

    // DESTRUCTOR
    /**
     * @brief Destroy the Mnt object
     * 
     */
    ~Mnt();

    //METHOD
    /**
     * @brief Get the Altitude By Coord object
     * 
     * @param coords 
     * @return double 
     */
    double getAltitudeByCoord(double E, double N);
};

#endif //MNT_H