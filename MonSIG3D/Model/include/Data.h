/**
 * @file Data.h
 * @author TSI
 * @brief
 * @version 0.1
 * @date 2021-11-18
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : Vertexes attributes and getters from Shapefile transfered to Data
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * Modifs : Add a color attribute
 * Maxime Charzat - 2021-11-24
 * Modifs : Add isExtrude attribute
 * Claire-Marie Alla, Maxime Charzat - 25/11/2021
 * Modifs : Add gdaldataset
 * Paul Miancien - 2021-11-25
 * Modifs : Add attributes for 3D vertex
 * Maxime Charzat, Théo Huard - 2021-11-29
 * Modifs : add fieldHauteur + setter, getter of Field3D
 * Claire-Marie-Marie Alla, Adrienne Zebaze 2021-11-29
 * Modifs : Add mntPath attribute
 * Maxime Charzat - 2021-12-01
 */

#ifndef __DATA_H__
#define __DATA_H__

#include <atomic>
#include <gdal/ogrsf_frmts.h>
#include <glm/glm.hpp>
#include <iostream>
#include <map>
#include <vector>
#include <gdal/ogrsf_frmts.h> 

using namespace std;

class Data 
{
    protected:
        /**
         * @brief id of the data
         * 
         */
        uint32_t id;

        /**
         * @brief name of the data
         * 
         */
        string name;

        /**
         * @brief Type of the data
         * 
         */
        string type;

        /**
         * @brief Path of the data
         * 
         */
        string path;

        /**
         * @brief gdalDataset
         */
        GDALDataset* gdalDataset;

        /**
         * @brief Vertex that describe the object
         *
         */
        vector<vector<glm::vec3>> vertex;

        /**
         * @brief Vertex that describe the point object
         *
         */
        vector<glm::vec3> vertexPoint;

        /**
         * @brief Vertex that describe the linear object
         *
         */
        vector<vector<glm::vec3>> vertexLine;

        /**
         * @brief Vertex that describe the extruded linear object
         *
         */
        vector<vector<glm::vec3>> vertexExtrudeLine;

        /**
         * @brief vertexTriangulateExtruded
         */
        std::vector<std::vector<glm::vec3>> vertexTriangulateExtruded = std::vector<std::vector<glm::vec3>>();

        /**
         * @brief Vertex that describe the contour of polygonal object
         *
         */
        vector<vector<glm::vec3>> vertexContour = std::vector<std::vector<glm::vec3>>();

        /**
         * @brief vertexContourExtruded
         */
        std::vector<std::vector<glm::vec3>> vertexContourExtruded = std::vector<std::vector<glm::vec3>>();

        /**
         * @brief Vertex that describe the polygonal object
         *
         */
        vector<vector<glm::vec3>> vertexPolygon = std::vector<std::vector<glm::vec3>>();

        /**
         * @brief size of the vertex
         *
         */
        unsigned int size;

        /**
        * @brief Say if the data is visible or not
        *
        */
        bool isVisible;

        /**
         * @brief isExtrude
         */
        bool isExtrude = false;

        /**
         * @brief isTriangulate
         */
        bool isTriangulate = false;

        std::map<int, glm::vec3> dictColor = {{0, glm::vec3(0.3f, 0.3f, 0.8f)},
                                              {1, glm::vec3(1.0f, 0.0f, 0.0f)},
                                              {2, glm::vec3(0.52f, 0.27f, 0.31f)},
                                              {3, glm::vec3(0.58f, 0.51f, 0.95f)},
                                              {4, glm::vec3(0.96f, 0.8f, 0.35f)},
                                              {5, glm::vec3(0.34f, 0.38f, 0.6f)}};
        /**
         * @brief color
         */
        glm::vec3 color = dictColor.at(id % dictColor.size());

        /**
         * @brief listOfFields
         * list of Fields of the layer
         */
        vector<string> listOfFields;



        const char* fieldHeight = "No height";

        /**
         * @brief name of specific layer
         */
        std::string typeName;

        std::string mntPath;

	/**
         * @brief extents
         */
        vector<double> extents;

        /**
         * @brief barycenter
         */
        vector<double> barycenter;

	

    public:
        /**
         * @brief Data
         * Construct a new Data's object
         * 
         * @param name 
         * @param path 
         * @param vertex 
         */
        Data(string name, string path);

        /**
         * @brief ~Data
         * Destroy the Data's object
         * 
         */
        ~Data();

        /**
         * @brief Get the Id's object
         * 
         * @return int 
         */
        int getId();

        /**
         * @brief Get the Name's object
         * 
         * @return string
         */
        string getName();

        /**
         * @brief Set the Name's object
         * 
         * @param name 
         */
        void setName(string newName);

        /**
         * @brief Get the Type's object
         * 
         * @return string
         */
        string getType();

        /**
         * @brief Set the Type's object
         * 
         * @param type 
         */
        void setType(string newType);

        /**
         * @brief Get the Path's object
         * 
         * @return string
         */
        string getPath();

        /**
         * @brief Set the Path's object
         * 
         * @param path 
         */
        void setPath(string newPath);

        /**
         * @brief Get the isVisible object
         *
         * @return true
         * @return false
         */
        bool getVisibility();

        /**
         * @brief Set the Is Visible object
         *
         * @param newVisibility
         */
        void setVisibility(bool newVisibility);

        /**
         * @brief Get the GdalDataset object
         *
         * @return true
         * @return false
         */
        GDALDataset* getGdalDataset();
        
        /**
         * @brief Set the GdalDataset object
         *
         * @param newVisibility
         */
        void setGdalDataset(GDALDataset* gdalDataset);
	
	/**
         * @brief getExtrude
         * @return
         */
        bool getExtrude() {return isExtrude;}

        /**
         * @brief setExtrude
         * @param newExtrude
         */
        void setExtrude(bool newExtrude) {this->isExtrude = newExtrude;}

        /**
         * @brief Get vertex for display with opengl
         *
         */
        vector<vector<glm::vec3>> getVertex(){return vertex;}

        /**
         * @brief setVertexPoint
         * @param newVertex
         */
        void setVertexPoint(vector<glm::vec3> newVertex){this->vertexPoint = newVertex;}


        /**
         * @brief Get vertex for display with opengl
         * @return std::vector<glm::vec3>
         */
        vector<glm::vec3> GetVertexPoint(){return vertexPoint;}

        /**
         * @brief setVertexPoint
         * @param newVertex
         */
        void setVertexLine(vector<vector<glm::vec3>> newVertex){this->vertexLine = newVertex;}


        /**
         * @brief Get vertex for display with opengl
         * @return std::vector<std::vector<glm::vec3>>
         */
        vector<vector<glm::vec3>> GetVertexLine(){return vertexLine;}

        /**
         * @brief Get vertex for display with opengl
         * @return std::vector<std::vector<glm::vec3>>
         */
        vector<vector<glm::vec3>> GetVertexExtrudeLine(){return vertexExtrudeLine;}
        /**
         * @brief setVertexPolygon
         * @param newVertex
         */
        void setVertexContour(vector<vector<glm::vec3>> newVertex){this->vertexContour = newVertex;}

        /**
         * @brief Get vertex for display with opengl
         * @return std::vector<std::vector<glm::vec3>>
         */
        vector<vector<glm::vec3>> GetVertexContour(){return vertexContour;}

        /**
         * @brief setVertexPoint
         * @param newVertex
         */
        void setVertexPolygon(vector<vector<glm::vec3>> newVertex){this->vertexPolygon = newVertex;}

        /**
         * @brief Get vertex for display with opengl
         * @return std::vector<glm::vec3>
         */
        vector<vector<glm::vec3>> GetVertexPolygon(){return vertexPolygon;}

        /**
         * @brief Get size of the vertex
         *
         * @return size
         */
        unsigned int getSize(){return size;}

        /**
         * @brief getColor
         * @return color
         */
        glm::vec3 getColor(){return color;}

        /**
         * @brief Get all fields of the shapefile
         * @return list of shapefile fields
         */
        vector<string> getListOfFields(){return listOfFields;}

        /**
         * @brief setField3D
         * @param field
         */
        void setField3D(const char* field){this->fieldHeight = field;}

        /**
         * @brief getField3D
         * @return
         */
        const char* getField3D(){return fieldHeight;}

        /**
         * @brief getTypeName
         * @return
         */
        string getTypeName(){return typeName;}

        /**
         * @brief clearAllVertex
         */
        void clearAllVertex();

	/**
         * @brief Set the extents of the object
         *
         * @param extents
         */
        void setExtents(vector<double> extents);

        /**
         * @brief Get the extents of the object
         *
         * @return extents
         */
        vector<double> getExtents(){return extents;}

        /**
         * @brief Calculate the extents of the object
         */
        virtual void calculateExtentsBarycenter() = 0;

        void setBarycenter(vector<double> newBarycenter);

//Tests

        friend class data_constructor_Test;
        friend class data_destructor_Test;
        friend class data_getId_Test;
        friend class data_getName_Test;
        friend class data_setName_Test;
        friend class data_getType_Test;
        friend class data_setType_Test;
        friend class data_getPath_Test;
        friend class data_getPath_Test;
        friend class data_setPath_Test;
        friend class data_getVisibility_Test;
        friend class data_setVisibility_Test;
        friend class data_color_Test;

};

/**
 * @brief global id
 *
 */
static std::atomic_uint32_t globalId;



#endif // __DATA_H__
