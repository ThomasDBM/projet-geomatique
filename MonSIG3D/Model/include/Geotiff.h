#ifndef GEOTIFF_H
#define GEOTIFF_H

#include <atomic>
#include <iostream>
#include <glm/glm.hpp>
#include <vector>
#include <string>
#include <Data.h>

using namespace std;

class Geotiff : public Data
{
    private:
    /**
     * @brief x0 : East coords of the top left pixel
     * 
     */
	double x0;

    /**
     * @brief y0: North coords of the top left pixel
     * 
     */
	double y0;

    /**
     * @brief Number of pixel in width
     * 
     */
	int w;

    /**
     * @brief Number of pixels in height
     * 
     */
	int h;

    /**
     * @brief Field resolution of a pixel in width
     * 
     */
	double rw;

    /**
     * @brief Field resolution of a pixel in height
     * 
     */
	double rh;

    public:
    // Constructor
    /**
	 * @brief Construct a new Geo Tiff object
	 * 
	 * @param m_name 
	 * @param m_path 
	 */
    Geotiff(string m_name,string m_path, string mntPath);

    // Destroyer
	/**
	 * @brief Destroy the Geo Tiff object
	 * 
	 */
	~Geotiff();

    // Methods
	/**
	 * @brief Function that generate the vertex of the GeoTif to send to the shader
	 * 
	 */
    void generateVertex();

    /**
     * @brief setVertex
     */
    void setVertex(vector<vector<glm::vec3>> newVertex);

    /**
     * @brief Calculate the extents of the object
     */
    void calculateExtentsBarycenter() override;


};


#endif // GEOTIFF_H
