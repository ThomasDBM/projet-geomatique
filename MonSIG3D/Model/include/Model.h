/**
 * @file Model.h
 * @author TSI
 * @brief
 * @version 0.1
 * @date 2021-11-18
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : Implement shapefile loading in addLayer
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * Modifs : add load WFS
 * Maïlys MONGE - 2021-11-26
 * Modifs : add the possibility to change the mntPath during loading a geotiff
 * Maxime Charzat - 2021-12-01
 */

#ifndef MODEL_H
#define MODEL_H

#include <iostream>
#include <vector>
#include <string>
#include <Data.h>
#include <Mnt.h>

using namespace std;

class Model {
    private:



    public:
    /**
     * @brief layers
     */
    vector<Data*> layers;
    // Constructor
    /**
     * @brief Construct a new Model object
     * 
     */
    Model();

    // Destructor
    /**
     * @brief Destroy the Model object
     * 
     */
    ~Model();

    /**
     * @brief Function that add a layer to the list of layer
     * 
     * @param m_type Type of the new layer
     * @param m_name Name of the new layer
     * @param m_path Path of the new layer
     *
     * @return int corresponding to loading status
     */
    int addLayer(string m_type, string m_name, string m_path, string mnt_path = "../MonSIG3D/data/GeoTIFF/Lyon_arr5/mnt_arr5.tif");

    /**
     * @brief Get the adresse of a layer depending on it id in the model
     * 
     * @param id id of the layer
     * @return Data* adress of the layer variable
     */
    Data* getLayerById(uint32_t id);

    /**
     * @brief Get the adresse of a layer depending on it name in the model
     * @param name of the layer
     * @return Data* adress of the layer variable
     */
    Data* getLayerByName(string name);

    /**
     * @brief Function that remove a layer from this model
     * 
     * @param id Id of the layer to remove
     * @return true 
     * @return false 
     */
    bool removeLayer(string name);

    /**
     * @brief Get the Number Of Layers 
     * 
     * @return int 
     */
    int getNumberOfLayers();

    /**
     * @brief Get the Layer By Position object
     * 
     * @param position Position of the layer in the vector Model
     * @return Data* Adresse of the layer
     */
    Data* getLayerByPosition(int position);

    /**
     * @brief checkName
     * @param name
     * @return The final name
     */
    string checkName(string name);

    /**
     * @brief move two item in layer of Data
     * @param name1
     * @param name2
     */
    void moveItem(string name1, string name2);

    /**
     * @brief getElementPositionByName
     * @param name
     * @return
     */
    int getElementPositionByName(string name);

    /**
     * @brief Model::loadInfoWFS
     * @param path
     * @return true or false if information are get
     */
    bool loadWFS(string path);

    /**
     * @brief getLayerStreamByPath
     * @param path
     * @return
     */
    vector<string> getLayerStreamByPath(string path);

    /**
     * @brief loadVertexLayerStream
     * @param name
     */
    void loadVertexLayerStream(string name);

    /**
     * @brief Model::clearVertex
     * @param name
     */
    void clearVertex(string name);

    vector<double> getMaxExtents();


    //tests
    friend class model_addLayerGeotiff_Test;
    friend class model_addLayerShapefile_Test;
    friend class model_wrongAddLayerShapefile_Test;
    friend class model_twoAddLayer_Test;
    friend class controller_loadShpGood_Test;
    friend class controller_loadShpBad_Test;
    friend class controller_loadTifGood_Test;
    friend class controller_loadTifBad_Test;

};

#endif // MODEL_H
