#ifndef POINTL93_H
#define POINTL93_H

#include <atomic>
#include <iostream>
#include <vector>
#include <string>

using namespace std;

class PointL93
{
public:
    /**
     * @brief east coordinate of the point
     *
     */
    double east;

    /**
     * @brief north coordinate of the point
     *
     */
    double north;
public:
    /**
     * @brief Construct a new PointL93 object
     *
     * @param m_east
     * @param m_north
     */
    PointL93(double m_east, double m_north);

    /**
     * @brief Destroy the PointL93 object
     *
     */
    ~PointL93();
};

#endif // POINTL93_H
