#ifndef WFS_H
#define WFS_H

/*! \class Wfs
    \brief Wfs class.
    \author MMohad
    \date 10/11/2021
    \details 
    \version 1.0
 */

#include <atomic>
#include <iostream>
#include <vector>

#include <Data.h>

#include <gdal/gdal.h>
#include <gdal/ogrsf_frmts.h>
#include <glm/glm.hpp>

class Wfs : public Data {
public:

    /**
     * @brief Construct a new Wfs object
     * 
     * @param name 
     * @param path 
     */
    Wfs(std::string name, std::string path,std::string m_typeName);

    /**
     * @brief Destroy the Wfs object
     * 
     */
    ~Wfs();

    /**
     * @brief Calculate the extents of the object
     */
    void calculateExtentsBarycenter() override;
    
    /**
     * @brief loadWfsMetaData
     * @param nameOfLayer
     * @return
     */
    bool loadWfsMetaData(std::string nameOfLayer);

    /**
     * @brief Get Projection of the shapefile
     * @return OGRSpatialReference
     */
    const char* GetProjection();


    /**
     * @brief Generate the Vertexes of the shapefile data
     */
    void generateVertex(string mPath);


    /**
     * @brief Calculate the Vertex of shapefile data containing points
     * @param geometry of the feature
     */
    void calculatePointVertex(OGRGeometry* geometry);

    /**
     * @brief Calculate the Vertex of a linestring of the shapefile data
     * @param geometry : geometry of the feature
     */
    void calculateLineVertex(OGRGeometry* geometry);

    /**
     * @brief Calculate the Vertex of a polygon of the shapefile data
     * @param geometry : geometry of the feature
     */
    void calculatePolygonVertex(OGRGeometry* geometry);

    /**
     * @brief setDatasetByName
     */
     void setDatasetSpatialRef();

     /**
     * @brief setter for the GDALDataset
     * @param newDataset
     */
    void setGDALDataset(GDALDataset* newDataset){this->gdalDataset = newDataset;}

    /**
     * @brief initializeVertex
     */
    void initializeVertex();
//tests
    
    friend class wfs_constructor_Test;
    friend class wfs_destructor_Test;
    friend class wfs_load_Test;
    friend class wfs_GetProjection_Test;
};



#endif // Wfs_H
