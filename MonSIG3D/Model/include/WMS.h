/* WMS.h
 * Create the WMS class who contains all information about a WMS from the model
 * Maïlys MONGE
 * First merge : 16/11/21
*/
#ifndef __WMS_H__
#define __WMS_H__

#include <iostream>
#include<vector>

#include "Data.h"

using namespace std;

/**
 * @brief WMS Class which inherits of Data
 * 
 */
class WMS : protected Data
{
private:
    /* data */
public:
    /**
     * @brief Construct a new WMS object
     * 
     * @param name  
     * @param path 
     * @param vertex
     */
    WMS(string name, string path);
    /**
     * @brief Destroy the WMS object
     * 
     */
    ~WMS();

    void generateVertex();
    bool getMetaHeader();

    /**
     * @brief Calculate the extents of the object
     */
    void calculateExtentsBarycenter() override;

    
//Tests
    friend class data_constructor_Test;
    friend class data_destructor_Test;
    friend class data_getId_Test;
    friend class data_getName_Test;
    friend class data_setName_Test;
    friend class data_getType_Test;
    friend class data_setType_Test;
    friend class data_getPath_Test;
    friend class data_setPath_Test;
    friend class data_getVertex_Test;
    friend class data_setVertex_Test;
    friend class data_getVisibility_Test;
    friend class data_setVisibility_Test;
    friend class wms_constructor_Test;
    friend class wms_destructor_Test;
    friend class wms_metaHeaderGood_Test;
    friend class wms_metaHeaderBad_Test;
    friend class data_color_Test;
    friend class data_setGdalDataset_Test;
    friend class data_getGdalDataset_Test;
};


#endif // __WMS_H__
