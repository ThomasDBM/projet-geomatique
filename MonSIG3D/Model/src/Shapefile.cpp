/* Shapefile.cpp
 * All methods of the shapefile class
 * Claire-Marie Alla, Axelle Gaigé, Maxime Charzat
 * First merge : 10/11/2021
 * Modifs : Add calculate2DLineVertex
 * Modifs : Change type of the vertex
 * Charzat Maxime - 15/11/2021
 * Modifs : Add verifications of extension shp
 * Vignaud Amaryllis - 15/11/2021
 * Modifs : Add reprojection method + 1 test
 * Claire-Marie Alla, Amaryllis Vignaud - 17/11/2021
 * Modifs : Add calculate2DPolygonVertex
 * Charzat Maxime - 16/11/2021
 * Modifs : Add verification of extension shp - 15/11/2021
 *          Add Get projection - 16/11/2021
 * Vignaud Amaryllis
 * Modifs : Refactor functions to combine the generation of 2D and 3D vertex
 * Charzat Maxime - 17/11/2021
 * Modifs : Update load function following update Shapefile class
 * Vignaud Amaryllis - 18/11/2021
 * Modifs : Vertexes attributes and getters transfered to Data
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * Modifs : Add functions to generate vertex with extrude elements
 * Claire-Marie Alla, Maxime Charzat - 25/11/2021
 * Modifs : Add functions get and set listOfFields for shapefile
 * Amaryllis Vignaud, Axelle Gaige - 2021-11-25
 * Modifs : Add function to triangulate polygons
 * Huard Théo, Maxime Charzat - 26/11/2021
 * Modifs : Add functions to generate and handle vertex for extruded geometries
 * Maxime Charzat, Théo Huard - 2021-11-29
 * Modifs : change generate vertex to have fieldHeight
 * Claire-Marie Alla - 2021-11-29
*/

#include <fstream>
#include <gdal/ogrsf_frmts.h>
#include <glm/glm.hpp>
#include <iostream>
#include <gdal/ogr_feature.h>
#include <regex>
#include <Shapefile.h>
#include <string>
#include <sstream>

Shapefile::Shapefile(string name, string path):Data(name, path){
    string type = "vector";
    this->type = type;
}

Shapefile::~Shapefile(){
}

const char* Shapefile::GetProjection(){

    //access to the layer
    OGRLayer * layer = gdalDataset->GetLayer(0);
    layer -> ResetReading();

    //access to features and geometry
    OGRGeometry * geom = layer->GetNextFeature()->GetGeometryRef();

    //access EPSG
    int EPSG = geom->getSpatialReference()->GetEPSGGeogCS();

    //convert to char
    std::string tmp = std::to_string(EPSG);
    const char * EPSG_str = tmp.c_str();

    return EPSG_str;
}

void Shapefile::ReprojectShp(){

     // Lambert 93 projection
     OGRSpatialReference *newEPSG = new OGRSpatialReference;
     newEPSG->importFromEPSG(2154);

     // Reprojection intial proj -> L93
     OGRLayer * layer = gdalDataset->GetLayer(0);
     OGRFeature * feature;
     layer -> ResetReading();

     // Browse all geometry
     while( (feature = layer->GetNextFeature()) != NULL ) {

         OGRGeometry *transformed {feature->GetGeometryRef()};
         transformed->transformTo(newEPSG);
         feature->SetGeometryDirectly(transformed);
         OGRErr success;
         success = layer->SetFeature(feature);
     }
     OSRDestroySpatialReference(newEPSG);
}

bool Shapefile::load(){

    GDALAllRegister();

    //path of obligatory files
    std::string s2 = std::regex_replace(path, std::regex("shp"), "dbf");
    std::string s3 = std::regex_replace(path, std::regex("shp"), "shx");

    //Verification of existing files
    std::ifstream shp(path);
    std::ifstream dbf(s2);
    std::ifstream shx(s3);
    if (!shp && !dbf && !shx)
    {
        std::cout<<"All files are missing !"<<std::endl;
        return false;
    }
    else if (!shp | !dbf | !shx)
    {
        std::cout<<"One or several files are missing !"<<std::endl;
        return false;
    }
    else {
        std::cout<<"The files exist"<<std::endl;

        // Verify extension
        if(path.substr(path.length()-3, path.length()-1) == "shp")
        {
            // Load the shapefile
            gdalDataset = (GDALDataset*) GDALOpenEx(path.c_str(), GDAL_OF_VECTOR | GDAL_OF_UPDATE, NULL, NULL, NULL );
            if( gdalDataset == NULL )
            {
                std::cout << "Open failed.\n";
                return false;
            }
            std::cout << "Open succeed.\n";
        }else{
            std::cout << "Wrong format.\n";
            return false;
        }
    }
    return true;
}

void Shapefile::initializeVertex(){
    vertexPoint.clear();
    vertexLine.clear();

    vertexPoint = std::vector<glm::vec3>();
    vertexLine = std::vector<std::vector<glm::vec3>>();

    vertexPolygon.swap(vertexTriangulateExtruded);
    vertexContour.swap(vertexContourExtruded);
}

void Shapefile::generateVertex(bool toExtrude){

    initializeVertex();

    double height;

    // Grab the first layer
    OGRLayer* layer = gdalDataset->GetLayer(0);

    OGRFeature *feature;

    layer->ResetReading();

    // Iterate all features of a layer
    while( (feature = layer->GetNextFeature()) != NULL )
    {
        int index = feature->GetFieldIndex(fieldHeight);
        if(index != -1) height = feature->GetFieldAsDouble(fieldHeight);

        OGRGeometry *geometry = feature->GetGeometryRef();

        if( geometry != NULL
                && wkbFlatten(geometry->getGeometryType()) == wkbPoint )
        {
            if (!toExtrude) this->calculatePointVertex(geometry);
            else this->calculatePointExtrudeVertex(geometry, height);
        }
        else if( geometry != NULL
                && wkbFlatten(geometry->getGeometryType()) == wkbMultiPoint )
        {
            //Get point data
            OGRMultiPoint* multiPoint = (OGRMultiPoint*) geometry;
            OGRGeometry* geompoint = multiPoint->getGeometryRef(0);

            if (!toExtrude) this->calculatePointVertex(geompoint);
            else this->calculatePointExtrudeVertex(geompoint, height);
        }
        else if( geometry != NULL
                && wkbFlatten(geometry->getGeometryType()) == wkbLineString )
        {
            if (!toExtrude) this->calculateLineVertex(geometry);
            if (!isTriangulate) this->calculateLineExtrudeVertex(geometry, height);
        }
        else if( geometry != NULL
                 && wkbFlatten(geometry->getGeometryType()) == wkbMultiLineString )
        {
            //Get number of geometry (ex nb of lines)
            OGRMultiLineString* multiline = (OGRMultiLineString*) geometry;
            int nbGeometry = multiline->getNumGeometries();

            for(int k = 0; k < nbGeometry; k++){
                OGRGeometry* geomLine = multiline->getGeometryRef(k);

                if (!toExtrude) this->calculateLineVertex(geomLine);
                if (!isTriangulate) this->calculateLineExtrudeVertex(geomLine, height);
            }
        }
        else if( geometry != NULL
                && wkbFlatten(geometry->getGeometryType()) == wkbPolygon
                && !isTriangulate)
        {

            OGRPolygon* polygon = (OGRPolygon*) geometry;
            OGRLineString* ligne = polygon->getExteriorRingCurve()->CurveToLine();

            vertexPolygon.push_back(std::vector<glm::vec3>());
            vertexTriangulateExtruded.push_back(std::vector<glm::vec3>());
            vertexTriangulateExtruded.push_back(std::vector<glm::vec3>());

            this->calculateTriangulation(polygon, height);
            this->calculateLineVertex(ligne, false);
            this->calculateLineExtrudeVertex(ligne, height, false);
        }
        else if( geometry != NULL
                 && wkbFlatten(geometry->getGeometryType()) == wkbMultiPolygon
                 && !isTriangulate)
        {
            //Get number of geometry (ex nb of polygons)
            OGRMultiPolygon* multipolygon = (OGRMultiPolygon*) geometry;
            int nbGeometry = multipolygon->getNumGeometries();

            for(int k = 0; k < nbGeometry; k++){
                OGRGeometry* geomPolygon = multipolygon->getGeometryRef(k);

                OGRPolygon* polygon = (OGRPolygon*) geomPolygon;
                OGRLineString* ligne = polygon->getExteriorRingCurve()->CurveToLine();

                vertexPolygon.push_back(std::vector<glm::vec3>());
                vertexTriangulateExtruded.push_back(std::vector<glm::vec3>());
                vertexTriangulateExtruded.push_back(std::vector<glm::vec3>());

                this->calculateTriangulation(polygon, height);
                this->calculateLineVertex(ligne, false);
                this->calculateLineExtrudeVertex(ligne, height, false);
            }
        }
        else
        {
        }
    }
    if (!isTriangulate){
        isTriangulate = true;
    }
}

void Shapefile::calculatePointVertex(OGRGeometry *geometry){
    //Get point data
    OGRPoint* point = (OGRPoint*) geometry;

    //Get coordinates
    double x = point->getX();
    double y = point->getY();
    double z = point->getZ();

    //Add vertex of the point to the list
    vertexPoint.push_back(glm::vec3(x, y, z));
}

void Shapefile::calculatePointExtrudeVertex(OGRGeometry *geometry, double h){
    //Get point data
    OGRPoint* point = (OGRPoint*) geometry;

    vertexLine.push_back(std::vector<glm::vec3>());

    //Get coordinates
    double x = point->getX();
    double y = point->getY();
    double z = point->getZ();

    //Add vertex of the point to the list
    vertexLine[vertexLine.size() - 1].push_back(glm::vec3(x, y, z));
    vertexLine[vertexLine.size() - 1].push_back(glm::vec3(x, y, z + h));
}

void Shapefile::calculateLineVertex(OGRGeometry* geometry, bool fillLine, bool fillPolygon, double h){
    OGRLineString* ligne = (OGRLineString*) geometry;
    int nbPoints = ligne->getNumPoints();

    // Declaration of a new line
    if (fillLine) vertexLine.push_back(std::vector<glm::vec3>());
    else if (fillPolygon) {
        nbPoints -= 1;
    }
    else vertexContour.push_back(std::vector<glm::vec3>());

    for(int k = 0; k < nbPoints; k++){
        //Get coordinates
        double x = ligne->getX(k);
        double y = ligne->getY(k);
        double z = ligne->getZ(k);

        //Add vertex of the point to the list
        if (fillLine) vertexLine[vertexLine.size() - 1].push_back(glm::vec3(x, y, z));
        else if (fillPolygon) {
            vertexPolygon[vertexPolygon.size() - 1].push_back(glm::vec3(x, y, z));
            vertexTriangulateExtruded[vertexTriangulateExtruded.size() - 2].push_back(glm::vec3(x, y, z));
            vertexTriangulateExtruded[vertexTriangulateExtruded.size() - 1].push_back(glm::vec3(x, y, z + h));
        }
        else vertexContour[vertexContour.size() - 1].push_back(glm::vec3(x, y, z));
    }
}

void Shapefile::calculateLineExtrudeVertex(OGRGeometry* geometry, double h, bool fillLine){
    OGRLineString* ligne = (OGRLineString*) geometry;
    int nbPoints = ligne->getNumPoints();

    // Declaration of a new line
    if (!fillLine) {
        vertexContourExtruded.push_back(std::vector<glm::vec3>());
        vertexContourExtruded.push_back(std::vector<glm::vec3>());
    }
    vertexExtrudeLine.push_back(std::vector<glm::vec3>());

    for(int k = 0; k < nbPoints; k++){
        //Get coordinates
        double x = ligne->getX(k);
        double y = ligne->getY(k);
        double z = ligne->getZ(k);

        //Add vertex of the point to the list
        vertexExtrudeLine[vertexExtrudeLine.size() - 1].push_back(glm::vec3(x, y, z));
        vertexExtrudeLine[vertexExtrudeLine.size() - 1].push_back(glm::vec3(x, y, z + h));
        if (!fillLine) {
            vertexContourExtruded[vertexContourExtruded.size() - (k + 2)].push_back(glm::vec3(x, y, z));
            vertexContourExtruded[vertexContourExtruded.size() - (k + 1)].push_back(glm::vec3(x, y, z + h));

            vertexContourExtruded.push_back(std::vector<glm::vec3>());
            vertexContourExtruded[vertexContourExtruded.size() - 1].push_back(glm::vec3(x, y, z));
            vertexContourExtruded[vertexContourExtruded.size() - 1].push_back(glm::vec3(x, y, z + h));
        }
    }
}

vector<string> Shapefile::getFields(){
    return listOfFields;
}

void Shapefile::setFields(){
    OGRLayer* layer = gdalDataset->GetLayer(0);
    int nbfeatures = layer->GetFeature(0)->GetFieldCount();
    for(int i = 0; i < nbfeatures-1; i++){
        OGRFieldDefn * feature = layer->GetFeature(0)->GetDefnRef()->GetFieldDefn(i);
        std::string s(feature->GetNameRef());
        listOfFields.push_back(s);
    }
}

void Shapefile::calculateTriangulation(OGRPolygon* polygon, double h, unsigned int i, unsigned int nbAttempt){
    OGRLineString* ligne = polygon->getExteriorRingCurve()->CurveToLine();

    int nbPoints = ligne->getNumPoints();

    //stop condition
    if (nbPoints == 4 ){
        calculateLineVertex(ligne, false, true, h);
        return ;
    }
    if (nbAttempt == nbPoints || !polygon->IsValid()) return;

    //Create Triangle
    OGRLineString triangle = OGRLineString();

    for(int k = i ; k < i + 3 ; k++){
        unsigned int number = k % (nbPoints - 1);
        triangle.addPoint((double)ligne->getX(number),
                          (double)ligne->getY(number),
                          (double)ligne->getZ(number));
    }
    triangle.addPoint(ligne->getX(i), ligne->getY(i), ligne->getZ(i));

    bool within = polygon->Contains( &triangle );

    //triangle is an "ear" -> keep it and reduce polygon
    if (within){
        OGRPolygon polygon_reduced = OGRPolygon();
        OGRLinearRing ligne_reduced = OGRLinearRing();
        for (int k = 0; k < nbPoints; k++){
            if (k != ((i + 1) % (nbPoints - 1))){
                ligne_reduced.addPoint((double)ligne->getX(k),
                                       (double)ligne->getY(k),
                                       (double)ligne->getZ(k));
            }
        }
        if (((i + 1) % (nbPoints - 1)) == 0){
            ligne_reduced.addPoint((double)ligne->getX(1),
                                   (double)ligne->getY(1),
                                   (double)ligne->getZ(1));
        }
        polygon_reduced.addRing(&ligne_reduced);

        calculateLineVertex(&triangle, false, true, h);
        calculateTriangulation(&polygon_reduced, h, (unsigned int) i % (nbPoints-2));

    }
    //triangle is not an "ear" -> continue with another triangle
    else {
        calculateTriangulation(polygon, h, (unsigned int) ((i+1) % (nbPoints-1)), nbAttempt + 1);
    }
}


OGRFeature * Shapefile::getFeatureFromClick(OGRPoint* clickedPoint)
{
    // Grab the first layer
    OGRLayer *layer = gdalDataset->GetLayer(0);
    layer->ResetReading();
    OGRFeature *feature;

    while( (feature = layer->GetNextFeature()) != NULL ){
        OGRGeometry *geometry = feature->GetGeometryRef();

        if(geometry != NULL && wkbFlatten(geometry->getGeometryType()) == wkbPolygon
                || wkbFlatten(geometry->getGeometryType()) == wkbMultiPolygon){

            if(geometry->Contains (clickedPoint)){
                return feature;
            }
        }

    }
    return nullptr;


}

vector<string> Shapefile::getContentFeature(OGRFeature* feature){

    // Grab the first layer
    OGRLayer *layer = gdalDataset->GetLayer(0);
    OGRFeatureDefn *FDefn = layer->GetLayerDefn();

    vector<string> listOfInfos;


    for( int iField = 0; iField < FDefn->GetFieldCount(); iField++ )
    {
        OGRFieldDefn *FieldDefn = FDefn->GetFieldDefn( iField );
        char complement[] = "NULL";
        string nameElement;
        nameElement = feature -> GetDefnRef()->GetFieldDefn(iField) -> GetNameRef();
        listOfInfos.push_back(nameElement);

        switch( FieldDefn->GetType() )
        {
            case OFTInteger:
                int element_to_add;
                element_to_add = feature ->GetFieldAsInteger( iField );
                if(element_to_add == NULL){
                    listOfInfos.push_back(complement);
                }
                else{
                    listOfInfos.push_back( to_string(element_to_add) );

                }
                break;
            default:
            if(feature->GetFieldAsString(iField) == NULL){
                listOfInfos.push_back(complement);
            }
            else{

                listOfInfos.push_back(feature->GetFieldAsString(iField) );
            }                break;
        }
    }
    return listOfInfos;
}

void Shapefile::calculateExtentsBarycenter(){
    //access to the layer
    OGRLayer * layer = gdalDataset->GetLayer(0);
    layer -> ResetReading();

    //access to features and geometry
    OGRGeometry * geom = layer->GetNextFeature()->GetGeometryRef();
    OGREnvelope boundaries;
    geom->getEnvelope(&boundaries);
    this->extents.push_back(boundaries.MinX);
    this->extents.push_back(boundaries.MaxY);
    this->extents.push_back(boundaries.MaxX);
    this->extents.push_back(boundaries.MinY);
    this->barycenter.push_back((boundaries.MinX+boundaries.MaxX)/2);
    this->barycenter.push_back((boundaries.MinY+boundaries.MaxY)/2);
}
