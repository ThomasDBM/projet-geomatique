/**
 * @file Wfs.cpp
 * @author Melodia Mohad
 * @brief Implementation method's Wfs
 * @version 0.1
 * @date 2021-11-17
 * 
 * @copyright Copyright (c) 2021
 * 
 */
#include "Wfs.h"
#include "Shapefile.h"
#include <Controller.h>
#include "gdal/gdal.h"
#include "gdal/cpl_string.h"
#include <iostream>
#include <regex>
#include <string>


Wfs::Wfs(std::string name, std::string path, std::string mTypeName): Data(name, path){

    std::string wfs = "Wfs";
    this->type = wfs;
    this->typeName = mTypeName;
}

Wfs::~Wfs(){

}

void Wfs::initializeVertex(){

    vertexPoint.clear();
    vertexLine.clear();
    vertexContour.clear();
    vertexPolygon.clear();

    vertexPoint = std::vector<glm::vec3>();
    vertexLine = std::vector<std::vector<glm::vec3>>();
    vertexContour = std::vector<std::vector<glm::vec3>>();
    vertexPolygon = std::vector<std::vector<glm::vec3>>();
}

 const char* Wfs::GetProjection(){

     //access to the layer
     OGRLayer * layer = gdalDataset->GetLayer(0);
     layer -> ResetReading();

     //access to features and geometry
     OGRGeometry * geom = layer->GetNextFeature()->GetGeometryRef();

     //access EPSG
     int EPSG = geom->getSpatialReference()->GetEPSGGeogCS();

     //convert to char
     std::string tmp = std::to_string(EPSG);
     const char * EPSG_str = tmp.c_str();

     return EPSG_str;
 }


 void Wfs::generateVertex(string mPath){

     cout<<"[Loading] generating the Vertex"<<endl;

     //Initialization
     initializeVertex();

     //Set the path with GetFeature Request and reprojection
     string newPath = regex_replace(mPath, regex("\\GetCapabilities"), "GetFeature&typename=" + typeName);
     newPath+="&SRSNAME=urn:ogc:def:crs:EPSG::2154";
     GDALDataset* pNewDataset = (GDALDataset*) GDALOpenEx(newPath.c_str(), GDAL_OF_VECTOR, NULL, NULL, NULL );
     this->setGDALDataset(pNewDataset);

     // Grab the first layer
     OGRLayer* layer = gdalDataset->GetLayer(0);

     OGRFeature *feature;

     layer->ResetReading();

     // Iterate all features of a layer
     while( (feature = layer->GetNextFeature()) != NULL )
     {
         OGRGeometry *geometry = feature->GetGeometryRef();

         if( geometry != NULL
                 && wkbFlatten(geometry->getGeometryType()) == wkbPoint )
         {
             this->calculatePointVertex(geometry);
         }
         else if( geometry != NULL
                 && wkbFlatten(geometry->getGeometryType()) == wkbMultiPoint )
         {
             //Get point data
             OGRMultiPoint* multiPoint = (OGRMultiPoint*) geometry;
             OGRGeometry* geompoint = multiPoint->getGeometryRef(0);
             this->calculatePointVertex(geompoint);
         }
         else if( geometry != NULL
                 && wkbFlatten(geometry->getGeometryType()) == wkbLineString )
         {
             this->calculateLineVertex(geometry);
         }
         else if( geometry != NULL
                  && wkbFlatten(geometry->getGeometryType()) == wkbMultiLineString )
         {
             //Get number of geometry (ex nb of lines)
             OGRMultiLineString* multiline = (OGRMultiLineString*) geometry;
             int nbGeometry = multiline->getNumGeometries();

             for(int k = 0; k < nbGeometry; k++){
                 OGRGeometry* geomLine = multiline->getGeometryRef(k);
                 this->calculateLineVertex(geomLine);
             }
         }
         else if( geometry != NULL
                 && wkbFlatten(geometry->getGeometryType()) == wkbPolygon )
         {
             this->calculatePolygonVertex(geometry);
         }
         else if( geometry != NULL
                  && wkbFlatten(geometry->getGeometryType()) == wkbMultiPolygon )
         {
             //Get number of geometry (ex nb of polygons)
             OGRMultiPolygon* multipolygon = (OGRMultiPolygon*) geometry;
             int nbGeometry = multipolygon->getNumGeometries();

             for(int k = 0; k < nbGeometry; k++){
                 OGRGeometry* geomPolygon = multipolygon->getGeometryRef(k);
                 this->calculatePolygonVertex(geomPolygon);
             }
         }
         else
         {
             printf( "no point geometry\n" );
         }
     }
 }

 void Wfs::calculatePointVertex(OGRGeometry *geometry){
     //Get point data
     OGRPoint* point = (OGRPoint*) geometry;

     //Get coordinates
     double x = point->getX();
     double y = point->getY();
     double z = point->getZ();

     //Add vertex of the point to the list
     vertexPoint.push_back(glm::vec3(x, y, z));
 }

 void Wfs::calculateLineVertex(OGRGeometry* geometry){
     OGRLineString* ligne = (OGRLineString*) geometry;
     int nbPoints = ligne->getNumPoints();

     // Declaration of a new line
     vertexLine.push_back(std::vector<glm::vec3>());

     for(int k = 0; k < nbPoints; k++){
         //Get coordinates
         double x = ligne->getX(k);
         double y = ligne->getY(k);
         double z = ligne->getZ(k);

        //Add vertex of the point to the list
         vertexLine[vertexLine.size() - 1].push_back(glm::vec3(x, y, z));
     }
 }

 void Wfs::calculatePolygonVertex(OGRGeometry* geometry){
     // Declaration of a new polygon
     vertexPolygon.push_back(std::vector<glm::vec3>());

     OGRMultiPolygon* triangulation = (OGRMultiPolygon*) geometry->DelaunayTriangulation(0.0, false);
     int nbGeometry = triangulation->getNumGeometries();

     for(int i = 0; i < nbGeometry; i++){
         // Declaration of a new triangle
         OGRPolygon* polygon = (OGRPolygon*) triangulation->getGeometryRef(i);
         OGRLineString* ligne = polygon->getExteriorRingCurve()->CurveToLine();

         int nbPoints = ligne->getNumPoints();
         for(int k = 0; k < nbPoints - 1; k++){
             //Get coordinates
             double x = ligne->getX(k);
             double y = ligne->getY(k);
             double z = ligne->getZ(k);

             //Add vertex of the point to the list
             int size = vertexPolygon.size() - 1;
             vertexPolygon[ size ].push_back(glm::vec3(x, y, z));
         }
     }
 }

void Wfs::calculateExtentsBarycenter(){
}
