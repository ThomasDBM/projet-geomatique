/* WMS.h
 * Implements the WMS methods from the WMS.h header
 * Maïlys MONGE
 * First merge : 16/11/21
*/

#include<iostream>
#include<vector>

#include "gdal/gdal_priv.h"
#include"WMS.h"

using namespace std;


WMS::WMS(string name, string path):Data(name, path){
    string wms = "WMS";
    this->type = wms;
}

WMS::~WMS(){
}



void WMS::generateVertex(){
    getMetaHeader();
}

bool WMS::getMetaHeader(){

    GDALAllRegister();
    GDALDataset* poDataset;
    const char* pathtest = path.c_str();
    poDataset =(GDALDataset*) GDALOpenEx(pathtest, GDAL_OF_RASTER, NULL, NULL, NULL );

    // Display error message and exit program if dataset fails to open correctly
    if( poDataset == NULL ){
        GDALClose( poDataset );
        throw invalid_argument ("Not a valid URL");
    }

    double adfGeoTransform[6];
    poDataset->GetGeoTransform(adfGeoTransform);

    // save infos
    double x0 = adfGeoTransform[0];
    double y0 = adfGeoTransform[3];

    double w = floor(poDataset->GetRasterXSize());
    double h = floor(poDataset->GetRasterYSize());

    double rw = adfGeoTransform[1];
    double rh = adfGeoTransform[5];

    double size = w*h*6*3*sizeof(double);

    return true;
}

void WMS::calculateExtentsBarycenter(){

}
