#include <iostream>
#include <string>
#include <Data.h>
#include <Geotiff.h>
#include <Pointl93.h>
#include <gdal/gdal_priv.h>
#include <Mnt.h>

using namespace std;

// Constructor
/**
 * @brief Construct a new Geotiff object
 * 
 * @param m_name 
 * @param m_path 
 */
Geotiff::Geotiff(string m_name, string m_path, string mntPath):Data(m_name, m_path)
{
    this->type = "raster";
    this->id = 1;
    this->mntPath = mntPath;
    generateVertex();
}

// Destroyer
/**
 * @brief Destroy the Geo Tiff object
 * 
 */
Geotiff::~Geotiff(){
}


// Methods
/**
 * @brief Function that generate the vertex of the Geotiff to send to the shader
 * 
 */
void Geotiff::generateVertex()
{
    Mnt mnt = Mnt(mntPath);

    // Get meta header of the file
    GDALDataset  *poDataset;
    GDALAllRegister();
    int n = path.length();
    char path2[n+1];
    strcpy(path2, path.c_str());
    poDataset = (GDALDataset *) GDALOpen( path2, GA_ReadOnly );

    double adfGeoTransform[6];
    poDataset->GetGeoTransform(adfGeoTransform);

    // save infos
    x0 = adfGeoTransform[0];
    y0 = adfGeoTransform[3];

    w = floor(poDataset->GetRasterXSize());
    h = floor(poDataset->GetRasterYSize());

    rw = adfGeoTransform[1];
    rh = adfGeoTransform[5];

    size = 2*w*h*13*sizeof(double);

    // generate Point
    vector<PointL93> points;
    for (int i = 0; i < h+1 ; i++){
        for (int j = 0; j < w+1; j++){
            double E = x0 + (double)j*rw;
            double N = y0 + (double)i*rh;
            points.push_back(PointL93(E , N));
        }
    }

    // Number of band verification
    int nbBand = poDataset->GetRasterCount();

    // Calcul of min and max of the band values
    int gray = 0;
    GDALRasterBand *bandg = poDataset->GetRasterBand(1);
    bandg->RasterIO(GF_Read,0,0,1,1,&gray,1,1,GDT_Int32,1,1,nullptr);
    int gMin, gMax = gray;
    if(nbBand==1){
        for (int i = 0; i < h  ; i++){
            for (int j = 0; j < w; j++){
                GDALRasterBand *bandg = poDataset->GetRasterBand(1);
                bandg->RasterIO(GF_Read,j,i,1,1,&gray,1,1,GDT_Int32,1,1,nullptr);
                if (gray<gMin)
                        gMin=gray;
                if (gray>gMax)
                        gMax=gray;
            }
        }
    }
    // generate triangle
    int cpt = 0;
    for (int i = 0; i < h  ; i++){
        for (int j = 0; j < w; j++){
            vertex.push_back(std::vector<glm::vec3>());

            // Number of band verification
            int nbBand = poDataset->GetRasterCount();
            
            if(nbBand==1){
                // init color
                int gray=0;

                // get color
                GDALRasterBand *bandg = poDataset->GetRasterBand(1);
                bandg->RasterIO(GF_Read,j,i,1,1,&gray,1,1,GDT_Int32,1,1,nullptr);
                // id of top left point of the pixel
                int ID = cpt;

                // First Compile for vertex
                // Color

                double G = ((double)gray - (double)gMin)/((double)gMax - (double)gMin);

                vertex[vertex.size() - 1].push_back(glm::vec3(points[ID].east, points[ID].north, mnt.getAltitudeByCoord(points[ID].east, points[ID].north)));
                vertex[vertex.size() - 1].push_back(glm::vec3(points[ID+1].east, points[ID+1].north, mnt.getAltitudeByCoord(points[ID+1].east, points[ID+1].north)));
                vertex[vertex.size() - 1].push_back(glm::vec3(points[ID+w+1].east, points[ID+w+1].north, mnt.getAltitudeByCoord(points[ID+w+1].east, points[ID+w+1].north)));
                vertex[vertex.size() - 1].push_back(glm::vec3(points[ID+w+2].east, points[ID+w+2].north, mnt.getAltitudeByCoord(points[ID+w+2].east, points[ID+w+2].north)));
                vertex[vertex.size() - 1].push_back(glm::vec3(G,G,G));
                cpt++;
            }

            if(nbBand==3){
            // init color
            int r=0;
            int g=0;
            int b=0;

            // get color
            GDALRasterBand *bandr = poDataset->GetRasterBand(1);
            GDALRasterBand *bandg = poDataset->GetRasterBand(2);
            GDALRasterBand *bandb = poDataset->GetRasterBand(3);

            bandr->RasterIO(GF_Read,j,i,1,1,&r,1,1,bandr->GetRasterDataType(),1,1,nullptr);
            bandg->RasterIO(GF_Read,j,i,1,1,&g,1,1,bandg->GetRasterDataType(),1,1,nullptr);
            bandb->RasterIO(GF_Read,j,i,1,1,&b,1,1,bandb->GetRasterDataType(),1,1,nullptr);


            // id of top left point of the pixel
            int ID = cpt;

            // First Compile for vertex
            // Color
            double R = (double)r/255;
            double G = (double)g/255;
            double B = (double)b/255;

            vertex[vertex.size() - 1].push_back(glm::vec3(points[ID].east, points[ID].north, mnt.getAltitudeByCoord(points[ID].east, points[ID].north)));
            vertex[vertex.size() - 1].push_back(glm::vec3(points[ID+1].east, points[ID+1].north, mnt.getAltitudeByCoord(points[ID+1].east, points[ID+1].north)));
            vertex[vertex.size() - 1].push_back(glm::vec3(points[ID+w+1].east, points[ID+w+1].north, mnt.getAltitudeByCoord(points[ID+w+1].east, points[ID+w+1].north)));
            vertex[vertex.size() - 1].push_back(glm::vec3(points[ID+w+2].east, points[ID+w+2].north, mnt.getAltitudeByCoord(points[ID+w+2].east, points[ID+w+2].north)));
            vertex[vertex.size() - 1].push_back(glm::vec3(R, G, B));



            cpt++;
            }
        }
        cpt++;
    }
}

void Geotiff::setVertex(vector<vector<glm::vec3>> newVertex){

    this->vertex = newVertex;

}

void Geotiff::calculateExtentsBarycenter(){
    this->extents.push_back(x0);
    this->extents.push_back(y0);
    this->extents.push_back(x0 + w*rw);
    this->extents.push_back(y0+ h*rh);
    this->barycenter.push_back((x0*2+ w*rw)/2);
    this->barycenter.push_back((y0*2+ h*rh)/2);
}

