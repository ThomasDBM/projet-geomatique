/**
 * @file Data.cpp
 * @author TSI
 * @brief
 * @version 0.1
 * @date 2021-11-18
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : implement setter and getter of Field3D
 * Claire-Marie-Marie Alla, Adrienne Zebaze 2021-11-29
 */

#include <Data.h>

using namespace std;

Data::Data(string name, string path):id(globalId++){

    this->name = name;
    this->path = path;
    this->isVisible = true;
}


Data::~Data(){
    gdalDataset = nullptr;
}

int Data::getId(){

    return this->id;

}

string Data::getName(){

    return this->name;

}

void Data::setName(string newName){

    this->name = newName;

}

string Data::getType(){

    return this->type;

}

void Data::setType(string newType){

    this->type = newType;

}

string Data::getPath(){

    return this->path;

}

void Data::setPath(string newPath){

    this->path = newPath;

}

bool Data::getVisibility(){
    
    return this->isVisible;

}

void Data::setVisibility(bool newVisibility){
    
    this->isVisible = newVisibility;

}

void Data::clearAllVertex(){
    vertexPoint.clear();
    vertexLine.clear();
    vertexContour.clear();
    vertexPolygon.clear();
}

GDALDataset* Data::getGdalDataset(){
    
    return this->gdalDataset;

}

void Data::setGdalDataset(GDALDataset* gdalDataset){
    
    this->gdalDataset = gdalDataset;

}

void Data::setExtents(vector<double> extents){

    this->extents = extents;
}

void Data::setBarycenter(vector<double> barycenter){

    this->barycenter = barycenter;
}


