/**
 * @file Model.cpp
 * @author TSI
 * @brief
 * @version 0.1
 * @date 2021-11-18
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : Implement shapefile loading in addLayer
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * Modifs : Implement moveItem
 * Monge Maïlys, Rouyer Nicolas - 2021-11-25
 * Modifs : remove generate vertex in load shp
 * Claire-Marie Alla - 2021-11-29
 * Modifs : add load WFS
 * Maïlys MONGE - 2021-11-26
 * Modifs : add the possibility to change the mntPath during loading a geotiff
 * Maxime Charzat - 2021-12-01
 */

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <sys/stat.h>
#include <vector>

#include <Geotiff.h>
#include <Model.h>
#include <Shapefile.h>
#include <Wfs.h>

using namespace std;

// Constructor
Model::Model(){

}

// Destructor
Model::~Model(){

    for (int i = 0; i < layers.size(); i++){
        delete layers[i];
        layers[i]=0;
        layers.clear();
    }

}

// Method
int Model::addLayer(string m_type, string m_name, string m_path, string mntPath){
    // Loading message
    cout << "[LOADING] " << m_name << " : " << m_path << endl;
    struct stat info;

    if(m_type == "wfs"){
            if(loadWFS(m_path)){
                cout << "[SUCCESS] " << m_name << " : " << m_path << " loaded !" << endl;
                return 0;
            }
            return 1;
    }else if (stat(m_path.c_str(), &info)!= 0){
        //path doesn't exist
        cout << "[FAILURE] " << m_name << " : " << m_path << " doesn't exist!" << endl;
        return 1;
    }else if (m_type == "geotiff"){
        // Creation of Geotiff object
        Geotiff* new_layer = new Geotiff(m_name, m_path, mntPath);
        cout << "[SUCCESS] " << m_name << " : " << m_path << " loaded !" << endl;

        // Add to the layer list
        layers.push_back(new_layer);
        return 0;

    } else if (m_type == "shapefile"){
        // Creation of Shapefile object
        Shapefile* new_layer = new Shapefile(m_name, m_path);
        if (new_layer->load()) {
            new_layer->setFields();
            cout << "[SUCCESS] " << m_name << " : " << m_path << " loaded !" << endl;

            // Add to the layer list
            layers.push_back(new_layer);
            return 0;
        }
        return 1;

    }

    else {
        //Error
        cout << "[FAILURE] " << m_name << " : " << m_path << " can't be loaded !" << endl;
        return 2;
    }
}

Data* Model::getLayerById(uint32_t id){
    //for all element in vector
    for (int i =0; i < layers.size();i++){
        if(layers[i]->getId() == id){
            return layers[i];
        }
    }
    return nullptr;
}

Data* Model::getLayerByName(string name){
    //for all element in vector
    for (int i =0; i < layers.size();i++){
        if(layers[i]->getName() == name){
            return layers[i];
        }
    }
    return nullptr;
}



string Model::checkName(string name){
    string nameFinal(name);
    int count(0);
    string underscore = "_";

    if(layers.size()==0){
        return nameFinal;
    }

    while(true){

        if(getLayerByName(nameFinal)){
            string strCount = to_string(count);
            nameFinal=name+underscore+strCount;
            count++;
        }
        else{
            return nameFinal;
        }

    }
    return nameFinal;
}

// A OPTIMISER
bool Model::removeLayer(string name){
    // Create the new layers
    vector<Data*> new_layers;

    //for all element in vector
    for (int i = 0; i < layers.size(); i++){
        if (layers[i]->getName() != name){
            new_layers.push_back(layers[i]);
        }
    }

    // if an element were remove
    if(layers.size() != new_layers.size()){
        layers.clear();
        layers = new_layers;
        new_layers.clear();
        return true;
    } else {
        return false;
    }
}


int Model::getNumberOfLayers(){
    return layers.size();
}

Data* Model::getLayerByPosition(int position){
    return (layers[position]);
}

void Model::moveItem(string name1, string name2){
    //Get position
    int positionName1 = this->getElementPositionByName(name1);
    int positionName2 = this->getElementPositionByName(name2);
    //swap
    std::swap(layers[positionName1],layers[positionName2]);
}



int Model::getElementPositionByName(string name){
    //for all element in vector
    for (int i =0; i < layers.size();i++){
        cout<<layers[i]->getName()<<endl;
        if(layers[i]->getName() == name){
            return i;
        }
    }
    throw invalid_argument ("The element with this name is not in the Model");
}

bool Model::loadWFS(string path){

    //Register all known configured GDAL drivers.
    GDALAllRegister();


     GDALDataset* poDS = (GDALDataset*) GDALOpenEx(path.c_str(), GDAL_OF_VECTOR, NULL, NULL, NULL );
     if( poDS == NULL ){
         return false;
      }
     for( int i=0;i< poDS->GetLayerCount();i++ ){

        Wfs *newLayer = new Wfs(poDS->GetLayer(i)->GetMetadataItem("Title") , path, poDS->GetLayer(i)->GetName());

        layers.push_back(newLayer);
    }

         std::cout << "Loading WFS layer by Name succeed.\n";
       return true;

}

vector<string> Model::getLayerStreamByPath(string path){
    vector<string> listLayerName;
    for (int i=0; i<layers.size();i++){
        if(layers[i]->getPath()==path){
            listLayerName.push_back(layers[i]->getName());
        }
    }
    return listLayerName;
}
vector<double> Model::getMaxExtents(){

    //initialize sorting variables
    int length = this->layers.size();
    double maximumX = 0;
    double maximumY = 0;
    double minimumX = 9999999999;
    double minimumY = 9999999999;

    for(int i=0;i<length;i++){

        this->layers[i]->calculateExtentsBarycenter();
        vector<double> extent = this->layers[i]->getExtents();

        if(extent[0]<minimumX){

            minimumX = extent[0];

        }
        if(extent[1]>maximumY){

            maximumY = extent[1];

        }
        if(extent[2]>maximumX){

            maximumX = extent[2];

        }
        if(extent[3]<minimumY){

            minimumY = extent[3];

        }
    }
    vector<double> vect{ minimumX,maximumY,maximumX,minimumY };
    return vect;
}

void Model::loadVertexLayerStream(string name){

    //get the right layer
    Data* layerToLoad = getLayerByName(name);
    Wfs layerCalculation(layerToLoad->getName(),layerToLoad->getPath(), layerToLoad->getTypeName());

    //Vertex calculation add set the Data object
    layerCalculation.generateVertex(layerToLoad->getPath());
    layerToLoad->setVertexPoint(layerCalculation.GetVertexPoint());
    layerToLoad->setVertexLine(layerCalculation.GetVertexLine());
    layerToLoad->setVertexPolygon(layerCalculation.GetVertexPolygon());
    layerToLoad->setVertexContour(layerCalculation.GetVertexContour());

    //Move the Data at the end of the layer
    int itemIndex = getElementPositionByName(name);
    auto it = layers.begin() + itemIndex;
    std::rotate(it, it + 1, layers.end());


}

void Model::clearVertex(string name){
     Data* layerToLoad = getLayerByName(name);
     layerToLoad->clearAllVertex();
}

