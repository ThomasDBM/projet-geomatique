#include <iostream>
#include <vector>
#include <gdal/gdal_priv.h>
#include <Mnt.h>
#include <math.h>


using namespace std;

// Constructor
Mnt::Mnt(string path){
    // Get meta header of the file
    GDALDataset  *poDataset;
    GDALAllRegister();
    int n = path.length();
    char path2[n+1];
    strcpy(path2, path.c_str());
    poDataset = (GDALDataset *) GDALOpen( path2, GA_ReadOnly );

    double adfGeoTransform[6];
    poDataset->GetGeoTransform(adfGeoTransform);

    // save infos
    x0 = adfGeoTransform[0];
    y0 = adfGeoTransform[3];

    w = floor(poDataset->GetRasterXSize());
    h = floor(poDataset->GetRasterYSize());

    rw = adfGeoTransform[1];
    rh = adfGeoTransform[5];

    size = 2*w*h*13*sizeof(double);

    // Number of band verification
    int nb_band = poDataset->GetRasterCount();

    // Generate Matrix
    for (int j = 0 ; j < h ; j++){
        vector<double> line;
        for (int i = 0 ; i < w ; i++){
            int height;
            // get color
            GDALRasterBand *band_h = poDataset->GetRasterBand(1);
            band_h->RasterIO(GF_Read,i,j,1,1,&height,1,1,GDT_Int32,1,1,nullptr);

            line.push_back((double)height);
        }
        mnt.push_back(line);
        line.clear();
    }
}

// Destructor
Mnt::~Mnt(){
    this->mnt.clear();
}

// Method
double Mnt::getAltitudeByCoord(double E, double N){ 
    // cout << E << " ; " << N << endl;
    // cout << x0 << " ; " << y0 << endl;
    // cout << x0 + w*rw << " ; " << y0 + rh*h << endl;
    if ((E < x0 + rw/2) || (E > x0 + rw*w - rw/2) || (N > y0 + rh/2) || (N < y0 + rh*h - rh/2 )){
        return 0.0;
    } else {
        double x = (E - (x0 + rw/2))/rw;
        double y = (y0 - N)/-rh;

        int a = (int)x;
        int b = (int)x+1;
        int c = (int)y;
        int d = (int)y+1;

        double dNO = sqrt((x-a)*(x-a) + (y-c)*(y-c));
        double dNE = sqrt((x-b)*(x-b) + (y-c)*(y-c));
        double dSO = sqrt((x-a)*(x-a) + (y-d)*(y-d));
        double dSE = sqrt((x-b)*(x-b) + (y-d)*(y-d));

        double h = (mnt[c][a]*dNO + mnt[c][b]*dNE + mnt[d][a]*dSO + mnt[d][b]*dSE)/(dNO+dNE+dSO+dSE);

        
        return h;

    }
}
