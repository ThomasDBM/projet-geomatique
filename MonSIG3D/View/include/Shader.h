/* Shader.h
 * Parse and Compile the shader
 * Claire-Marie Alla
 * Adaption to Qt
 * Charzat Maxime
 * First merge : 23/11/2021
*/

#pragma once


#include <glm/glm.hpp>
#include <QOpenGLExtraFunctions>
#include <string>
#include <unordered_map>

struct ShaderProgramSource
{
	std::string VertexSource;
	std::string FragmentSource;
};

class Shader: protected QOpenGLExtraFunctions
{
private:
    /**
     * @brief mFilePath
     */
    std::string mFilePath;

    /**
     * @brief mRendererID
     */
    unsigned int mRendererID;

    /**
     * @brief mUniformLocationCache
     */
    std::unordered_map<std::string, unsigned int> mUniformLocationCache;// caching for uniforms

public:

    /**
     * @brief Constructor
     */
    Shader();

    /**
     * @brief Constructor
     */
    Shader(const std::string& filePath);

    /**
     * @brief Destructor
     */
	~Shader();

    /**
     * @brief Initialize the OpenGL function for the class
     */
    void initializeShader();

    /**
     * @brief Bind a shader to the element that will be drawn
     */
    void bind();
    /**
     * @brief Unbind a shader
     */
    void unBind();

    /**
     * @brief Set an uniform of 1 int
     * @param name
     * @param value
     */
    void setUniforms1i(const std::string& name, int value);

    /**
     * @brief Set an uniform of 1 float
     * @param name
     * @param value
     */
    void setUniforms1f(const std::string& name, float value);

    /**
     * @brief Set an uniform of 4 float
     * @param name
     * @param v0
     * @param v1
     * @param v2
     * @param v3
     */
    void setUniforms4f(const std::string& name, float v0, float v1, float v2, float v3);

    /**
     * @brief Set an uniform of a matrix of float
     * @param name
     * @param matrix
     */
    void setUniformMat4f(const std::string& name, const glm::mat4& matrix);

private:

    /**
     * @brief Parse the shader file
     * @param filepath
     * @return code of the shader
     */
    ShaderProgramSource parseShader(const std::string& filepath);

    /**
     * @brief CompileShader
     * @param type
     * @param source
     * @return id of the shader
     */
    unsigned int compileShader(unsigned int type, const std::string& source);
    /**
     * @brief CreateShader
     * @param vertexShader
     * @param fragmentShader
     * @return m_RendererID
     */
    unsigned int createShader(const std::string& vertexShader, const std::string& fragmentShader);

    /**
     * @brief GetUniformLocation
     * @param name
     * @return
     */
    int getUniformLocation(const std::string& name);
};

