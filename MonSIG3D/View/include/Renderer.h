/* Renderer.h
 * Render the elements
 * Claire-Marie Alla
 * Adaption to Qt
 * Charzat Maxime
 * First merge : 23/11/2021
*/

#pragma once

#include <IndexBuffer.h>
#include <VertexArray.h>
#include <Shader.h>

#include <QOpenGLExtraFunctions>

class Renderer: protected QOpenGLExtraFunctions
{

public:

    /**
     * @brief Constructor
     */
    Renderer() {}

    /**
     * @brief Destructor
     */
    ~Renderer() {}

    /**
     * @brief Initialize the OpenGL function for the class
     */
    void initializeRenderer();

    /**
     * @brief Clear the openGL window
     */
    void clear();

    /**
     * @brief Draw the elements in the window
     * @param geomType : the geometry to draw
     * @param va : the vertex to draw
     * @param ib : the associated indexes
     * @param shader
     */
    void draw(GLenum geomType, VertexArray& va, IndexBuffer& ib, Shader& shader);
};
