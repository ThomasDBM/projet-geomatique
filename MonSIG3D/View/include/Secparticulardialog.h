/**
 * @file SecParticularDialog
 * @author AZebaze, Claire-Marie Alla
 * @brief Create the window to select 2.5D field
 * @version 1.0
 * @date 2021-11-25
 *
 *
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : Add combo field and getfieldHauteur
 * AZebaze, Claire-Marie Alla - 2021-11-29
**/

#ifndef SECPARTICULARDIALOG_H
#define SECPARTICULARDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QString>
#include <QWidget>
#include <Controller.h>
#include <mainwindow.h>

namespace Ui {
class SecParticularDialog;
}

class SecParticularDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SecParticularDialog(Controller *controller, QWidget *parent = nullptr);
    ~SecParticularDialog();

private:
    Ui::SecParticularDialog *ui;

    /**
     * @brief controller
     * controller that will link the view to the model
     */
    Controller * controller;


public slots:

    /**
     * @brief comBoField
     * choose field where height is used
     */
    void comboField();

    /**
     * @brief getCurrentTextCombo
     * get value of selected fiel in combobox
     */
    void getFieldHauteur();

};

#endif // SECPARTICULARDIALOG_H
