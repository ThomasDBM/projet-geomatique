/*! \class SecDialog
    \brief Create the window to select layer
    \author AZebaze
    \date 23/11/2021
    \details
    \version 1.0
 */

#ifndef SECDIALOG_H
#define SECDIALOG_H

#include <QDialog>
#include <QPushButton>
#include <QToolBar>
#include <QVBoxLayout>
#include <QWidget>

namespace Ui {
    class SecDialog;
}

class SecDialog : public QDialog
{

    Q_OBJECT

public:

    /**
     * @brief SecDialog
     * @param parent
     * @param tab
     */
    explicit SecDialog(QWidget *parent = nullptr, QString tab = QString("Vector"));

    /**
     * @brief destructor
     *
     */
    ~SecDialog();


private:

    /**
     * @brief Main interface for the SecDialog
     *
     */
    Ui::SecDialog *ui;

    /**
     * @brief Type of the last layer entered
     *
     */
    QString lastLayerType;

    /**
     * @brief Name of the last layer entered
     *
     */
    QString lastLayerName;

    /**
     * @brief Path of the las layer entered
     *
     */
    QString lastLayerPath;







public slots:

    /**
     * @brief open a fileDialog to get the vector files
     *
     */
    void openFileDialogVector();

    /**
     * @brief open a fileDialog to get the raster files
     *
     */
    void openFileDialogRaster();

    /**
     * @brief open a fileDialog to get the cityGML files
     *
     */
    void openFileDialogCityGML();

    /**
     * @brief When the cancel button is clicked, close the dialog and delete the layer's info
     */
    void cancel();

    /**
     * @brief add the vector layer
     *
     */
    void addVectorLayer();

    /**
     * @brief add the raster layer
     *
     */
    void addRasterLayer();

    /**
     * @brief add the cityGML layer
     *
     */
    void addCityGMLLayer();

    /**
     * @brief add the layer by flow
     *
     */
    void addFlowLayer();


    /**
     * @brief get the type of the selected layer
     * @return the type of the selected layer
     */
    QString getType();

    /**
     * @brief get the Name of the selected layer
     * @return the name of the selected layer
     */
    QString getName();

    /**
     * @brief get the path of the selected layer
     * @return the path of the selected layer
     */
    QString getPath();


};

#endif // SECDIALOG_H
