/* IndexBuffer.h
 * Describe and manage the index buffer inspired by the Cherno
 * Claire-Marie Alla
 * Adaption to Qt
 * Charzat Maxime
 * First merge : 23/11/2021
*/

#pragma once

#include <QOpenGLExtraFunctions>

class IndexBuffer: protected QOpenGLExtraFunctions
{
private:
    /**
     * @brief mRendererID
     */
    unsigned int mRendererID;

    /**
     * @brief mCount : index's number
     */
    unsigned int mCount;

public:

    /**
     * @brief Constructor
     * @param data: index to draw
     * @param count: index's number
     */
	IndexBuffer(const unsigned int* data, unsigned int count);

    /**
     * Destructor
     */
	~IndexBuffer();

    /**
     * @brief Bind an index buffer to the element that will be drawn
     */
    void bind();

    /**
     * @brief Unbind an index buffer
     */
    void unbind();

    /**
     * @brief getCount
     * @return mCount
     */
    inline unsigned int getCount() const { return mCount; }
};


