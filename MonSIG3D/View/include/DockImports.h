#ifndef DOCKIMPORTS_H
#define DOCKIMPORTS_H

/**\class DockImport
  *\brief Create the dock that will manage the imports
  *\author LJeannest
  *\date 19/11/2021
  *\details
  *\version 1.0
  * Modifs : function addWFS()
  * Monge Maïlys 2021-11-29
*/

#include <QBoxLayout>
#include <QListWidget>
#include <QToolBar>
#include <QFileSystemModel>
#include <QTreeView>

class DockImports : public QWidget
{
public:
    /**
     * @brief DockImports
     * Constructor for DockImports
     */
    DockImports();

    /**
     * @brief ~DockImports
     * Destructor for DockImports
     */
    ~DockImports();

    /**
     * @brief getter for the stream list
     * @return
     */
    QListWidget* getListStream(){return listStream;}

    /**
     * @brief add WFS layer name in the WFS view
     * @param name
     */
    void addWFS(QString name);


private:


    /**
     * @brief layoutImport
     * Layout of the import dock
     */
    QVBoxLayout *layoutImport;

    /**
     * @brief listImport
     * List in which the files will be displayed
     */
    QListWidget *listImport;

    /**
     * @brief treeView
     * treeView in which the files will be displayed
     */
    QTreeView *treeView;

    /**
     * @brief toolBarImports
     * toolbar linked to the imports
     */
    QToolBar * toolBarImports;

    /**
     * @brief action_Import_add
     * Action to add a new import
     */
    QAction * action_Import_add;

    /**
     * @brief action_Import_search
     * Action to search for a file locally
     */
    QAction * action_Import_search;

    /**
     * @brief action_Import_shrink
     * Action to shrink all the files displayed in the list
     */
    QAction * action_Import_shrink;

    /**
     * @brief action_Import_refresh
     * Action to refresh the import list
     */
    QAction * action_Import_refresh;

    /**
     * @brief QFileSystemModel
     * QFileSystemModel that will handle file display and managing
     */
    QFileSystemModel *dirmodel;

    /**
     * @brief treeView
     * treeView in which the files will be displayed
     */
    QListWidget *listStream;

};

#endif // DOCKIMPORTS_H
