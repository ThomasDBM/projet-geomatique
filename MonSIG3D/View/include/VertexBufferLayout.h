/* VertexBuffer.cpp
 * Describe and manage the vertex buffer layout
 * Claire-Marie Alla
 * Adaption to Qt
 * Charzat Maxime
 * First merge : 23/11/2021
*/


#pragma once

#include <vector>
#include <QOpenGLExtraFunctions>

struct VertexBufferElement
{

    /**
     * @brief type
     */
	unsigned int type;

    /**
     * @brief count
     */
	unsigned int count;

    /**
     * @brief normalized
     */
	unsigned char normalized;


    /**
     * @brief GetSizeOfType
     * @param type
     * @return int
     */
    static unsigned int getSizeOfType(unsigned int type)
	{
		switch (type)
		{
			case GL_FLOAT:			return 4;
			case GL_UNSIGNED_INT:	return 4;
			case GL_UNSIGNED_BYTE:	return 1;
		}
        Q_ASSERT(false);
                return 0;
	}
};


class VertexBufferLayout: protected QOpenGLExtraFunctions{

private:

    /**
     * @brief mElements
     */
    std::vector<VertexBufferElement> mElements;

    /**
     * @brief mStride
     */
    unsigned int mStride;

public:

    /**
     * @brief Constructor
     */
    VertexBufferLayout();

    /**
     * @brief Destructor
     */
    ~VertexBufferLayout();

    /**
     * @brief push
     * @param count
     */
    void push(unsigned int count);

    /**
     * @brief GetElements
     * @return
     */
    inline const std::vector<VertexBufferElement> getElements() const { return mElements; }

    /**
     * @brief GetStride
     * @return
     */
    inline unsigned int getStride() const { return mStride; }
};

