/**
 * @file MainWindow.h
 * @author TSI, LJeannest
 * @brief Create the main window
 * @version 0.1
 * @date 2021-11-08
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : Handle the scale
 * Axelle Gaigé, Théo Huard - 2021-11-24
 * Modifs : Add 3 methods to show particular window
 * Claire-Marie Alla - 2021-11-29
 * Modifs : fix docklayer function to handle WFS stream
 * Monge Maïlys, 2021-11-29

 */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <DockImports.h>
#include <DockLayers.h>
#include <Controller.h>

#include <QBoxLayout>
#include <QComboBox>
#include <QListWidget>
#include <QMainWindow>
#include <QToolBar>
#include <QPushButton>
#include <QFileSystemModel>
#include <QDialog>
#include <QOpenGLWidget>
#include <secdialog.h>
#include <Secparticulardialog.h>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    /**
     * @brief MainWindow
     * Constructor for MainWindow
     *
     * @param parent
     * @param linked controller
     */
    MainWindow(Controller * controller, QWidget *parent = nullptr);

    /**
     * @brief ~MainWindow
     * Destructor for MainWindow
     */
    ~MainWindow();



private:

    /**
     * @brief first time resize
     */
    bool firstTimeResize = true;
    QVector<double> qv_x, qv_y;
    /**
     * @brief ui
     * Main interface for the MainWindow
     */
    Ui::MainWindow *ui;

    /**
     * @brief Dock that will manage everything linked with the imports
     *
     */
    DockImports * dockImports;

    /**
     * @brief Dock that will manage everything linked with the layers
     *
     */
    DockLayers * dockLayers;

    /**
     * @brief resize Event
     */
    void resizeEvent(QResizeEvent*);

    /**
     * @brief update Scale
     */
    void updateScale();

    /**
     * @brief controller that will link the view to the model
     */
    Controller * controller;


    /**
     * @brief window where the layers are selected
     */
    SecDialog *sDialog;


    /**
     * @brief atpress
     * Declare variable when pressed coords
     */
    int xAtPress, yAtPress;




public slots:
    /**
     * @brief Keeps the buttons on the openGLWidget at the same position whenever the window is resized.
     *
     * @return 0
     */
    int moveButtons();

    /**
     * @brief move layer in the docklayer list one position below
     *
     */
    void moveLayerUp();

    /**
     * @brief move layer in the docklayer list one position above
     *
     */
    void moveLayerDown();

    /**
     * @brief remove layer in the docklayer list and the associated data whith his name
     */
    void removeLayer();

    /**
     * @brief setVisibility
     * update the view with the changed visibility
     * @param itemChanged
     */
    void setVisibility(QListWidgetItem* itemChanged);

    /**
     * @brief Zoom in
     */
    void zoomIn();

    /**
     * @brief Zoom out
     */
    void zoomOut();

    /**
     * @brief Provide the documentation file for users
     */
    void viewDocumentation();

    /**
     * @brief show window where layers are selected on the vector tab
     */
    void showWindowsLayersVect();

    /**
     * @brief show window where layers are selected on the raster tab
     *
     */
    void showWindowsLayersRast();

    /**
     * @brief show window where layers are selected on the flow tab
     *
     */
    void showWindowsLayersFlow();

    /**
     * @brief show window where layers are selected on the cityGML tab
     *
     */
    void showWindowsLayersCityGML();

    /**
     * @brief get the layers info once the secDialog is closed
     *
     */
    bool getLayerInfo();


    /**
     * @brief wheel event
     * @param event
     */
    void wheelEvent(QWheelEvent *event);

    /**
     * @brief switchOverXD
     */
    void switchOverXD();

    /**
     * @brief updateView
     */
    void updateView();

    void loadVertexLayerFlow(QListWidgetItem* item);
    void loadFlow(string path, string name);
    void showParticularWindowsLayers();
    void updateLayer();

};

#endif // MAINWINDOW_H
