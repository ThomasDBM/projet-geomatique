/*! \class DockLayers
    \brief Create the dock that will manage the layers
    \author LJeannest
    \date 19/11/2021
    \details
    \version 1.0
 */

#ifndef DOCKLAYERS_H
#define DOCKLAYERS_H

#include <QBoxLayout>
#include <QListWidget>
#include <QToolBar>

class DockLayers : public QWidget
{

public:
    /**
     * @brief Constructor for DockLayers
     *
     */
    DockLayers();

    /**
     * @brief Destructor for DockLayers
     *
     */
    ~DockLayers();

    /**
     * @brief get the list of Layers
     * @return the list of the layers
     *
     */
    QListWidget* getListLayers();

    /**
     * @brief get the button to up a layer
     * @return the button to up a layer
     *
     */
    QAction* getActionLayerUp();

    /**
     * @brief get the button to down a layer
     * @return the button to down a layer
     *
     */
    QAction* getActionLayerDown();

    /**
     * @brief get the button to delete a layer
     * @return the button to delete a layer
     *
     */
    QAction* getActionLayerDelete();

    /**
     * @brief add layer to the docklayer list
     * @param aLayer : name of layer
     *
     */
    void addLayer(QString aLayer);

private:
    /**
     * @brief Layout of the dock Layers
     *
     */
    QVBoxLayout *layoutLayers;

    /**
     * @brief List in which the files will be displayed
     *
     */
    QListWidget *listLayers;

    /**
     * @brief toolbar linked to the layers
     *
     */
    QToolBar * toolBarLayers;

    /**
     * @brief Action to raise a layer above the others
     *
     */
    QAction * actionLayersUp;

    /**
     * @brief Action to lower a layer under the others
     *
     */
    QAction * actionLayersDown;

    /**
     * @brief Action to delete a layer
     *
     */
    QAction * actionLayersDelete;

//Tests
    friend class docklayer_getListLayer_Test;


};

#endif // DOCKLAYERS_H
