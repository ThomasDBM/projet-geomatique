/* Glwidget.h
 * Overide class QOpenglwidget
 * Théo Huard, Axelle Gaigé, Maxime Charzat
 * First merge : 18/11/2021
 * Modifs : Implement resize (not complete)
 * Charzat Maxime Théo Huard Axelle Gaigé - 19/11/2021
 * Modifs : Implement functions to draw a shapefile
 * Charzat Maxime - 22/11/2021
 * Modifs : Implements functions to handle pan and zoom
 * Charzat Maxime, Axelle Gaigé, Théo huard, Amaryllis Vignaud - 24/11/2021
 * Modifs : Retrieve the controller
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * Modifs : Handle the scale
 * Axelle Gaigé, Théo Huard - 2021-11-24
 * Modifs : Add the scale attribute
 * Maxime Charzat - 2021-11-25
 * Modifs : Change attributes of position Camera & Center to be vec3
 * Axelle Gaige - 26/11/2021
 * Modifs : Change camera 2D to 3D
 * Amaryllis Vignaud, Axelle Gaigé - 2021-11-26
 * Modifs : Update rotation in 3D, fix pan in 3D, fix switch 2D/3D
 * Axelle Gaigé - 2021/11/28
 * Modifs : Add function to draw extruded lines
 * Maxime Charzat - 2021-11-29
 * Modifs : Function mousePressEvent
 * Charles Laverdure, Théo Huard, Maxime Charzat - 29-11-24
 * Modifs : change the default zoom value
 * Maxime Charzat - 2021-12-01
*/

#ifndef GLWIDGET_H
#define GLWIDGET_H
#define GLM_ENABLE_EXPERIMENTAL

#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtx/norm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/gtc/quaternion.hpp>
#include <glm/gtx/quaternion.hpp>
#include <IndexBuffer.h>
#include <map>
#include <QMouseEvent>
#include <QOpenGLExtraFunctions>
#include <QOpenGLWidget>
#include <QPoint>

#include <IndexBuffer.h>
#include <Renderer.h>
#include <Shader.h>
#include <Shapefile.h>
#include <VertexArray.h>
#include <VertexBuffer.h>
#include <VertexBufferLayout.h>

#include <Controller.h>
#include <Data.h>
#include <Shapefile.h>
#include <QQuaternion>

#define RGB_MIN 1
#define RGB_MAX 255

class GlWidget : public QOpenGLWidget, public QOpenGLExtraFunctions
{

private:

    /**
     * @brief pointer to the controller
     */
    Controller* controller;

    vector<glm::vec2> position;
    /**
     * @brief last Position
     */
    QPoint lastPos;
    OGRPoint clickPoint;
    glm::vec3 positionCenter = glm::vec3(839531.0f,6519145.0f,0.0f);
    glm::vec3 positionCamera = glm::vec3(839531.0f,6519145.0f,391.108*pow(2, 6));
    glm::fquat quaternion;

    /**
     * @brief fov (not to be changed)
     */
    float fov = 45.0f; //Do not change please (it was chosen to have nice zoom level)

    /**
     * @brief scale
     */
    double scale;
    glm::vec3 up = glm::vec3(0,1,0);
    double ratioPixL93;
    glm::mat4 view;

    /**
     * @brief proj
     */
    glm::mat4 proj;

    /**
     * @brief model
     */
    glm::mat4 model;

    /**
     * @brief matrix model view projection
     */
    glm::mat4 mvp;

    /**
     * @brief renderer
     */
    Renderer renderer;

    /**
     * @brief shader
     */
    Shader shader;
    VertexArray va;
    /**
     * @brief Transform a QColor to rgb values
     *
     * @param C : color to transform
     * @param r : red value
     * @param g : green value
     * @param b : blue value
     */

    void qColortoRGB(const QColor &C, float &r, float &g, float &b) const;
    /**
     * @brief Normalize a value between 0 and 1
     *
     * @param val : value to normalize
     * @param min : minimum value
     * @param max : maximum value
     */
    float normalize0_1(float val, float min, float max) const;

    /**
     * @brief Organize the painting of a raster layer
     *
     * @param rasterLayer : layer from raster data
     */
    void paintRasterLayer(Data* rasterLayer);

    /**
     * @brief Organize the painting of a vector layer
     *
     * @param vectorLayers : layer from vector data
     */
    void paintVectorLayer(Data* vectorLayers);

    /**
     * @brief Create the vector buffer for a point geometry
     *
     * @param vertex : Contain the coordinates of points features
     * @param size : number of points features
     * @param layout : description of the data format
     * @param geomType : the geometry to draw
     */
    void paintVectorPoint(std::vector<glm::vec3> vertex, int size, VertexBufferLayout &layout, GLenum geomType = GL_POINTS);

    /**
     * @brief Create the vector buffer for a line or a polygon geometry
     *
     * @param vertex : Contain the coordinates of lines or polygons features
     * @param size : number of features
     * @param layout : description of the data format
     * @param geomType : the geometry to draw
     */
    void paintVectorLinePolygon(std::vector<std::vector<glm::vec3>> vertex, int size, VertexBufferLayout &layout, GLenum geomType = GL_LINE_STRIP);
    /**
     * @brief Create the vector buffer for an extruded line
     *
     * @param vertex : Contain the coordinates of extruded lines features
     * @param size : number of features
     * @param layout : description of the data format
     * @param geomType : the geometry to draw
     */
    void paintVectorExtrudedLines(std::vector<std::vector<glm::vec3>> vertex, int size, VertexBufferLayout &layout, GLenum geomType = GL_TRIANGLES);
    /**
     * @brief Draw the features
     *
     * @param size : number of elements
     * @param geomType : the geometry to draw
     */
    void drawElement(int size, GLenum geomType);

    /**
     * @brief mousePressEvent
     * @param event
     */
    void mousePressEvent(QMouseEvent* event);

    /**
     * @brief mouseMoveEvent
     * @param event
     */
    void mouseMoveEvent(QMouseEvent* event);

    /**
     * @brief GlWidget::wheelEvent
     * @param event
     */
    void wheelEvent(QWheelEvent *event);

    /**
     * @brief calculateQuaternion
     * @param angle
     * @param axe
     * @return Quaternion
     */
    void calculateQuaternion(float x, float y, float z, float angle);

    /**
     * @brief rotateCamera
     * @param axeX : coordinate in x of rotation axe
     * @param axeY : coordinate in y of rotation axe
     * @param axeZ : coordinate in z of rotation axe
     * @param angle of rotation
     */
    void rotateCamera(float axeX, float axeY, float axeZ, float angle);

    /**
     * @brief pan3D
     * @param dx
     * @param dy
     * @param coeff
     */
    void pan3D(int dx, int dy, float coeff);


public:
    // Constructor
    /**
     * @brief Construct a new GlWidget object
     *
     */
    GlWidget(QWidget *parent);
    // Constructor
    /**
     * @brief Construct a new GlWidget object
     *
     */
    GlWidget();
    // Destructor
    /**
     * @brief Destroy the GlWidget object
     *
     */
    ~GlWidget();

    /**
     * @brief setController
     * @param c
     */
    void setController(Controller* c) {controller = c;}
    /**
     * @brief zoom
     * @param sign define if we zoom out (+1) or in (-1)
     */
    void zoom(int sign);


    /**
     * @brief calculateScale
     */
    string calculateScale();

    /**
     * @brief switch to 3D view
     */
    void switch3D();

    /**
     * @brief switch to 2D view
     */
    void switch2D();

    /**
     * @brief get point info
     */
    vector<string> infoFromPt;

protected:
    /**
     * @brief Initialize the OpenGL window
     *
     */
    void initializeGL() override;
    /**
     * @brief Rezise the OpenGL window, Set the camera
     *
     * @param w : width of the window
     * @param h : heigth of the window
     */
    void resizeGL(int w, int h) override;
    /**
     * @brief Paint the elements in the OpenGL window
     *
     */
    void paintGL() override;

    /**
     * @brief definViewMatrix
     */
    void defineViewMatrix();

};

#endif // GLWIDGET_H
