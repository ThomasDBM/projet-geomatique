/* VertexArray.h
 * Describe and manage the vertex array
 * Claire-Marie Alla
 * Adaption to Qt
 * Charzat Maxime
 * First merge : 23/11/2021
*/

#pragma once

#include <QOpenGLExtraFunctions>
#include <VertexBuffer.h>

class VertexBufferLayout;

class VertexArray: protected QOpenGLExtraFunctions
{

private:

    /**
     * @brief mRendererID
     */
    unsigned int mRendererID;

public:

    /**
     * @brief Constructor
     */
	VertexArray();

    /**
     * @brief Destructor
     */
    ~VertexArray();

    /**
     * @brief Initialize the OpenGL function for the class
     */
    void initializeVertexArray();

    /**
     * @brief add a buffer to the vertex array
     * @param vb : buffer that contain the data
     * @param layout : Description of the buffer
     */
    void addBuffer(VertexBuffer& vb, const VertexBufferLayout& layout);

    /**
     * @brief Bind a vertex array to the element that will be drawn
     */
    void bind();

    /**
     * @brief Unbind a vertex array
     */
    void unbind();
};

