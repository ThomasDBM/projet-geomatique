/* VertexBuffer.h
 * Describe and manage the vertex buffer
 * Claire-Marie Alla
 * Adaption to Qt
 * Charzat Maxime
 * First merge : 23/11/2021
*/

#pragma once

#include <QOpenGLExtraFunctions>

class VertexBuffer: protected QOpenGLExtraFunctions
{

private:

    /**
     * @brief mRendererID
     */
    unsigned int mRendererID;

public:

    /**
     * @brief Constructor
     * @param data
     * @param size : size of the data in memory
     */
	VertexBuffer(const void* data, unsigned int size);

    /**
     * @brief Destructor
     */
	~VertexBuffer();

    /**
     * @brief bind a vertex buffer to the element that will be drawn
     */
    void bind();

    /**
     * @brief unbind a vertex buffer
     */
    void unbind();
};


