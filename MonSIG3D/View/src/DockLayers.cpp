/*! \class DockLayers
    \brief Create the dock that will manage the layers
    \author LJeannest
    \date 19/11/2021
    \details
    \version 1.0
     * Modifs : add layer function : addLayer and get.....
     * Rouyer Nicolas - 2021-11-23
     * Modifs : add checkbox in item
     * Rouyer Nicolas - 2021-11-26
 */

#include "DockLayers.h"
#include <iostream>
#include <QApplication>
#include <QBoxLayout>
#include <QListWidget>
#include <QToolBar>
#include <QWidget>
#include <QApplication>

#include <iostream>

DockLayers::DockLayers() : QWidget(){

    toolBarLayers = new QToolBar();
    //toolBarLayers->setLayoutDirection(Qt::LeftToRight);

    //create the buttons to put in the toolBar
    actionLayersDelete = new QAction();
    QPixmap del (":/icon/layers/png_icons_sig/gestion_couches/retirer.png");
    actionLayersDelete->setIcon(del);
    actionLayersDelete->setToolTip("Delete");

    actionLayersUp = new QAction();
    QPixmap up (":/icon/layers/png_icons_sig/gestion_couches/fleche_h.png");
    actionLayersUp->setIcon(up);
    actionLayersUp->setToolTip("Up");

    actionLayersDown = new QAction();
    QPixmap down (":/icon/layers/png_icons_sig/gestion_couches/fleche_b.png");
    actionLayersDown->setIcon(down);
    actionLayersDown->setToolTip("Down");

    //add buttons to the toolbar
    toolBarLayers->addAction(actionLayersDelete);
    toolBarLayers->addAction(actionLayersUp);
    toolBarLayers->addAction(actionLayersDown);

    layoutLayers = new QVBoxLayout();

    listLayers = new QListWidget();

    layoutLayers->addWidget(toolBarLayers);
    layoutLayers->addWidget(listLayers);

    this->setLayout(layoutLayers);


}

DockLayers::~DockLayers(){
    delete toolBarLayers;
    delete actionLayersDelete;
    delete actionLayersUp;
    delete actionLayersDown;
    delete layoutLayers;
    delete listLayers;
}

QListWidget* DockLayers::getListLayers(){
    return listLayers;
}

QAction* DockLayers::getActionLayerDelete(){
    return actionLayersDelete;
}


QAction* DockLayers::getActionLayerUp(){
    return actionLayersUp;
}

QAction* DockLayers::getActionLayerDown(){
    return actionLayersDown;
}

void DockLayers::addLayer(QString aLayer){
    //create a new item in the list widget with the name of layer
    QListWidgetItem *newLayer = new QListWidgetItem;
    newLayer->setText(aLayer);
    //add checkbox to item
    newLayer->setFlags(newLayer->flags() | Qt::ItemIsUserCheckable);
    newLayer->setCheckState(Qt::Checked);

    //add checkbox to item
    newLayer->setFlags(newLayer->flags() | Qt::ItemIsUserCheckable);
    newLayer->setCheckState(Qt::Checked);

    //insert item
    listLayers->addItem(newLayer);
}
