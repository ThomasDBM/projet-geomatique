/* Glwidget.cpp
 * Overide class QOpenglwidget
 * Théo Huard, Axelle Gaigé, Maxime Charzat
 * First merge : 18/11/2021
 * Modifs : Implement resize (not complete)
 * Charzat Maxime Théo Huard Axelle Gaigé - 19/11/2021
 * Modifs : Implement functions to draw a shapefile
 * Charzat Maxime - 22/11/2021
 * Modifs : Implements functions to handle pan and zoom
 * Charzat Maxime, Axelle Gaigé, Théo huard, Amaryllis Vignaud - 24/11/2021
 * Modifs : Ask the controller to know the layers to paint
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * Modifs : Handle the scale
 * Axelle Gaigé, Théo Huard - 2021-11-24
 * Modifs : Add contour of polygons and retrieve colors from data
 * Maxime Charzat - 2021-11-24
 * Modifs : Implement function to draw a raster
 * Charzat Maxime - 24/11/2021
 * Modifs : Adjust the pan with the scale
 * Maxime Charzat - 2021-11-25
 * Modifs : Change the layout for 2D and 3D
 * Claire-Marie Alla, Maxime Charzat - 25/11/2021
 * Modifs : Change method zoom to work also in 3D
 * Axelle Gaige - 26/11/2021
 * Modifs : Change camera 2D to 3D
 * Amaryllis Vignaud, Axelle Gaigé - 2021-11-26
 * Modifs : Update rotation in 3D, fix pan in 3D, fix switch 2D/3D
 * Axelle Gaigé - 2021/11/28
 * Modifs : Adapt the drawing to the new vertex and to 2,5D
 * Maxime Charzat, Théo Huard - 2021-11-29
*/

#include <Glwidget.h>
#include <Model.h>
#include <QScreen>

#include <math.h>
#include <iostream>
#include <fstream>
#include <glm/gtx/quaternion.hpp>
#include <glm/gtx/string_cast.hpp>
#include <QDebug>
#include <QMouseEvent>
#include <QMessageBox>
#include <QString>
#include <mainwindow.h>

using namespace std;

GlWidget::GlWidget(QWidget *parent) : QOpenGLWidget(parent){
}

GlWidget::GlWidget(){
    // No OpenGL resource initialization is done here.
}

GlWidget::~GlWidget()
{
    // Make sure the context is current and then explicitly
    // destroy all underlying OpenGL resources.
    makeCurrent();

    doneCurrent();
}

void GlWidget::qColortoRGB(const QColor &C, float &r, float &g, float &b) const
{
    r=normalize0_1(C.red(),RGB_MIN,RGB_MAX);
    g=normalize0_1(C.green(),RGB_MIN,RGB_MAX);
    b=normalize0_1(C.blue(),RGB_MIN,RGB_MAX);
}

float GlWidget::normalize0_1(float val, float min, float max) const
{
    return (val-min)/(max-min);
}

void GlWidget::initializeGL()
{
    float r,g,b,a = normalize0_1(10.0f,RGB_MIN,RGB_MAX);
    initializeOpenGLFunctions();
    qColortoRGB(Qt::white, r,g,b);
    glClearColor(r,g,b,a);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glColorMaterial(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE);
    glEnable(GL_COLOR_MATERIAL);

    glPointSize(5);

    shader.initializeShader();
    renderer.initializeRenderer();
    va.initializeVertexArray();

    calculateScale();
}

void GlWidget::resizeGL(int w, int h)
{
    glViewport(0,0,w,h);

    defineViewMatrix();

    // Model matrix : an identity matrix (model will be at the origin)
    model = glm::mat4(1.0);

    float width = (float) w;
    float height = (float) h;
    proj = glm::perspective(glm::radians(fov), width/height, 0.1f, 100000.0f);
}

void GlWidget::paintGL()
{
    makeCurrent();

    mvp = proj * view * model;
    shader.bind();
    shader.setUniformMat4f("u_MVP", mvp);
    shader.unBind();

    renderer.clear();

    vector<Data*> layers = controller->getLayersToPaint();
    int nbLayers = layers.size();
    if (nbLayers != 0){
        for (int k = 0; k < nbLayers; k++){
            Data* layer = layers[k];
            std::string type = layer->getType();
            if (type == "vector"){
                this->paintVectorLayer(layer);
            }
            else if (type == "Wfs"){
                this->paintVectorLayer(layer);
            }

            else if (type == "raster"){
                this->paintRasterLayer(layer);
            }
        }
    }
    doneCurrent();
}

void GlWidget::paintRasterLayer(Data* rasterLayer){
    std::vector<std::vector<glm::vec3>> vertex = rasterLayer->getVertex();

    VertexBufferLayout layout;
    if (controller->getDisplay3D()) layout.push(3);
    else {
        layout.push(2);
        layout.push(1);
    }

    unsigned int indices[] = {
        0, 1, 2,
        1, 2, 3
    };

    IndexBuffer ib(indices, 6);
    ib.unbind();

    int size = vertex.size();
    if (size != 0){
        for (int k = 0; k < size; k++){
            glm::vec3 color = vertex[k][vertex[k].size() - 1];
            shader.bind();
            shader.setUniforms4f("u_Color", color[0], color[1], color[2], 1.0f);
            shader.unBind();

            int nbPoints = vertex[k].size();
            VertexBuffer vb(vertex[k].data(), 3 * nbPoints * sizeof(float));
            va.addBuffer(vb, layout);

            va.unbind();

            renderer.draw(GL_TRIANGLES, va, ib, shader);
        }
    }
}

void GlWidget::paintVectorLayer(Data* vectorLayers){

    std::vector<glm::vec3> vertexPoint = vectorLayers->GetVertexPoint();
    std::vector<std::vector<glm::vec3>> vertexLine = vectorLayers->GetVertexLine();
    std::vector<std::vector<glm::vec3>> vertexPolygon = vectorLayers->GetVertexPolygon();

    int sizePoints = vertexPoint.size();
    int sizeLines = vertexLine.size();
    int sizePolygons = vertexPolygon.size();

    glm::vec3 color = vectorLayers->getColor();

    shader.bind();
    shader.setUniforms4f("u_Color", color[0], color[0], color[2], 1.0f);
    shader.unBind();

    VertexBufferLayout layout;
    if (controller->getDisplay3D()){
        layout.push(3);
        std::vector<std::vector<glm::vec3>> vertexExtrudedLine = vectorLayers->GetVertexExtrudeLine();
        int sizeExtrudedLine = vertexExtrudedLine.size();
        if (sizeExtrudedLine != 0){
            paintVectorExtrudedLines(vertexExtrudedLine, sizeExtrudedLine, layout, GL_TRIANGLES);
        }
    }
    else {
        layout.push(2);
        layout.push(1);
    }

    if (sizePoints != 0){
        paintVectorPoint(vertexPoint, sizePoints, layout, GL_POINTS);
    }
    if (sizeLines != 0){
        paintVectorLinePolygon(vertexLine, sizeLines, layout, GL_LINE_STRIP);
    }
    if (sizePolygons != 0){
        std::vector<std::vector<glm::vec3>> vertexContour = vectorLayers->GetVertexContour();

        shader.bind();
        shader.setUniforms4f("u_Color", 0.0f, 0.0f, 0.0f, 1.0f);
        shader.unBind();

        paintVectorLinePolygon(vertexContour, vertexContour.size(), layout, GL_LINE_STRIP);

        shader.bind();
        shader.setUniforms4f("u_Color", color[0], color[0], color[2], 1.0f);
        shader.unBind();

        paintVectorLinePolygon(vertexPolygon, sizePolygons, layout, GL_TRIANGLES);
    }
}

void GlWidget::paintVectorPoint(std::vector<glm::vec3> vertex, int size, VertexBufferLayout &layout, GLenum geomType){

    VertexBuffer vb(vertex.data(), 3 * size * sizeof(float));
    va.addBuffer(vb, layout);

    this->drawElement(size, geomType);
}

void GlWidget::paintVectorLinePolygon(std::vector<std::vector<glm::vec3>> vertex, int size, VertexBufferLayout &layout, GLenum geomType){

    for (int k = 0; k < size; k++){
        int nbPoints = vertex[k].size();
        VertexBuffer vb(vertex[k].data(), 3 * nbPoints * sizeof(float));
        va.addBuffer(vb, layout);

        this->drawElement(nbPoints, geomType);
    }
}

void GlWidget::paintVectorExtrudedLines(std::vector<std::vector<glm::vec3>> vertex, int size, VertexBufferLayout &layout, GLenum geomType){

    for (int k = 0; k < size; k++){
        int nbPoints = vertex[k].size();
        VertexBuffer vb(vertex[k].data(), 3 * nbPoints * sizeof(float));
        va.addBuffer(vb, layout);

        unsigned int indices[((nbPoints - 2) * 3)];
        for (int i = 0; i < (nbPoints - 2); i++) {
            indices[3 * i] = i;
            indices[(3 * i) + 1] = i + 1;
            indices[(3 * i) + 2] = i + 2;
        }

        IndexBuffer ib(indices, ((nbPoints - 2) * 3));

        va.unbind();
        ib.unbind();

        renderer.draw(geomType, va, ib, shader);
    }
}

void GlWidget::drawElement(int size, GLenum geomType){

    unsigned int indices[size];
    for (int i = 0; i<size; i++) indices[i] = i;

    IndexBuffer ib(indices, size);

    va.unbind();
    ib.unbind();

    renderer.draw(geomType, va, ib, shader);
}

void GlWidget::mousePressEvent(QMouseEvent *event){

    if (event->buttons() & Qt::LeftButton) {
        lastPos = event->pos();

         // give the coordinates of the click relative to the center of the screen
         double xCoordClick = (event->pos().x())-(this->size().width())/2;
         double yCoordClick = (this->size().height()-5)/2 -(event->pos().y());
         double xCoordL93Click = positionCenter.x + ratioPixL93 * xCoordClick;
         double yCoordL93Click = positionCenter.y + ratioPixL93 * yCoordClick;
         clickPoint = OGRPoint(xCoordL93Click, yCoordL93Click);
         infoFromPt = controller->getPointInfo(&clickPoint);

         if(infoFromPt.size() != 0){
             string message;
             int p = 0;
             for (basic_string<char> i: infoFromPt){
                 p=p+1;
                 if(p % 2 == 0){
                    message.append(i);
                    message.append("\n");
                 }
                 else{
                     message.append(i);
                     message.append("   :   ");
                 }
             }

             QString qstr = QString::fromStdString(message);
             QMessageBox::information(
                         this,
                         tr("Selection's Information"),
                         qstr);
         }
         else{
         }

    }

}

void GlWidget::mouseMoveEvent(QMouseEvent *event){
    int dx = event->x() - lastPos.x();
    int dy = event->y() - lastPos.y();
    lastPos = event->pos();


    if (event->buttons() & Qt::LeftButton) {
        float coeff = 1 / (5000 * scale);
        if (controller->getDisplay3D()){
            pan3D(dx, dy , coeff); //Pan in 3D
        }
        else {
            positionCenter[0] -= coeff * dx;
            positionCenter[1] += coeff * dy;

            positionCamera[0] -= coeff * dx;
            positionCamera[1] += coeff * dy;
        }
    }
    else if (event->buttons() & Qt::RightButton){
        if ( controller->getDisplay3D()){
            //Rotate camera in 3D
            rotateCamera(0, 0, 1, dx*M_PI/180); //Rotate camera around center (z=constant)
            glm::vec3 axe = glm::cross(positionCenter - positionCamera, up); //Calculate axe around which to turn when mouse move in y
            axe = normalize(axe); //Normalize axe
            rotateCamera(axe[0], axe[1], axe[2], dy*M_PI/180); //Rotate camera aroud axe
        }
    }
    defineViewMatrix();
    update();
}


void GlWidget::wheelEvent(QWheelEvent *event){
    QPoint numDegrees = event->angleDelta() / 8;

    if (!numDegrees.isNull()) {
        QPoint numSteps = (-1) * numDegrees / 15;
        zoom(numSteps.y());
    }
}

void GlWidget::zoom(int sign){
    float norm = l2Norm(positionCamera,positionCenter); //
    glm::vec3 directionCenterToCam = (positionCamera-positionCenter)/norm;

    positionCamera = positionCenter + directionCenterToCam * norm * (float) pow(2.0f, sign);

    defineViewMatrix();
    update();
}

void GlWidget::defineViewMatrix(){
    view = glm::lookAt(
                    positionCamera, // Camera is at (xCenter,yCenter,zCam), in World Space
                    positionCenter, // and looks at the origin
                    up  // Head is up (set to 0,-1,0 to look upside-down)
                    );
}

string GlWidget::calculateScale(){
//    Use next line to calculate where to place camera to have nice scale
//    float z = heightMeterWidget * 2000 / (2*tan((fov/2.0f)*M_PI/180.0f));

    int height = this->size().height()-5; //Get height of glwidget

    double nbOfPixPerInchY = physicalDpiY(); //Calculate number of pixel per inch
    double pixelSizeInchY = 1/nbOfPixPerInchY; //Calculate pixel size in inch
    double pixelSizeMeterY = pixelSizeInchY * 0.0254; //Calculate pixel size in meter
    double heightMeterWidget = pixelSizeMeterY * (double)height; //Calculate height of widget in meter

    double heightGeographic = 2 * tan((fov/2.0f)*M_PI/180.0f) * positionCamera[2]; //Calculate height of widget in world coordinates
    ratioPixL93 = heightGeographic/height;
    string scaletext;
    scale = heightMeterWidget/heightGeographic;  //Calculate scale

    if (scale > 1){
        scaletext = std::to_string((int)round(scale))+" : 1"; //Define scale
    }
    else {
        double denominateur = 1/scale;
        scaletext = "1 : " + std::to_string((int)round(denominateur)); //Define scale
    }
    return(scaletext);
}

void GlWidget::switch3D(){
    rotateCamera(-1, 0, 0, 25); //Rotate camera along -x axe of 45degree
    defineViewMatrix(); //Update view matrix
}

void GlWidget::switch2D(){
    positionCamera[0] = positionCenter[0]; //Update camera vector for 2D mode
    positionCamera[1] = positionCenter[1];
    up[0]= 0;   //Update up vector for 2D mode
    up[1]= 1;
    up[2]= 0;
    defineViewMatrix(); //Update view matrix
}

void GlWidget::calculateQuaternion(float x, float y, float z, float angle){
    //Calculate all variables defining quaternion
    float a = x * sin(angle/2*M_PI/180);
    float b = y * sin(angle/2*M_PI/180);
    float c = z * sin(angle/2*M_PI/180);
    float d = cos(angle/2*M_PI/180);
    quaternion = glm::fquat(d,a,b,c); //Update quaternion
    quaternion = normalize(quaternion); //Normalize quaternion to use it after
}


void GlWidget::rotateCamera(float axeX, float axeY, float axeZ, float angle){
    //Calculate quaternion defining the rotation
    calculateQuaternion(axeX, axeY, axeZ, angle);

    //Calculate tranlation matrix to translate data from center to the frame's origin
    glm::fmat4 translateIni = glm::fmat4(1.0);
    translateIni [0][3]= -positionCenter[0];
    translateIni [1][3]= -positionCenter[1];

    //Calculate rotation matrix
    glm::fmat4 rotationMatrix = toMat4(quaternion);

    //Calculate tranlation matrix to translate data back from frame's origin to center
    glm::fmat4 translateBack = glm::fmat4(1.0);
    translateBack [0][3]= positionCenter[0];
    translateBack [1][3]= positionCenter[1];

    //Calculate the full transform matrix
    glm::fmat4 transform = translateIni * rotationMatrix *translateBack ;
    //Defiine a vector4 representing camera position
    glm::fvec4 cam4pos = glm::vec4(positionCamera[0],positionCamera[1],positionCamera[2], 1.0f);
    //Rotate camera
    glm::fvec4 camBack = cam4pos * transform;
    //Update camera position
    positionCamera = glm::vec3(camBack[0], camBack[1], camBack[2]);

    //Defiine a vector4 representing camera orientation
    glm::fvec4 up4 = glm::vec4(up[0], up[1], up[2], 1.0f);
    //Rotate up vector (orientation of camera)
    up4 = up4 * rotationMatrix;
    //Update camera orientation
    up = glm::vec3(up4[0], up4[1], up4[2]);
}

void GlWidget::pan3D(int dx, int dy, float coeff){
    //Calculate axes to translate along
    glm::vec3 axeY = positionCenter -positionCamera; //Axe when moving mouse on x
    axeY = normalize(axeY); //Normalize axe
    glm::vec3 axeX = glm::cross(up, positionCenter - positionCamera); //Axe when moving mouse on x
    axeX = normalize(axeX); //Normalize axe

    //Define translation matrix when moving mouse on x
    glm::mat4 translationDy = glm::mat4(1.0);
    translationDy [0][3]= dy * coeff *axeY[0];
    translationDy [1][3]= dy * coeff *axeY[1];

    //Define translation matrix when moving mouse on y
    glm::mat4 translationDx = glm::mat4(1.0);
    translationDx [0][3]= dx * coeff *axeX[0];
    translationDx [1][3]= dx * coeff *axeX[1];

    glm::mat4 transform = translationDx * translationDy; //Calculate transformation matrix

    //Move center
    glm::vec4 center = glm::vec4(positionCenter[0], positionCenter[1], positionCenter[2], 1.0);
    center = center * transform;
    positionCenter[0] = center[0];
    positionCenter[1] = center[1];

    //Move camera
    glm::vec4 camera = glm::vec4(positionCamera[0], positionCamera[1], positionCamera[2], 1.0);
    camera = camera * transform;
    positionCamera[0] = camera[0];
    positionCamera[1] = camera[1];
}
