/* IndexBuffer.cpp
 * Describe and manage the index buffer inspired by the Cherno
 * Claire-Marie Alla
 * Adaption to Qt
 * Charzat Maxime
 * First merge : 23/11/2021
*/

#include <IndexBuffer.h>


IndexBuffer::IndexBuffer(const unsigned int* data, unsigned int count)
    :mCount(count)
{
    initializeOpenGLFunctions();

    // verified that unsigned int has the same size that GLuint
    Q_ASSERT(sizeof(unsigned int) == sizeof(GLuint));

    glGenBuffers(1, &mRendererID); // 1 for 1 buffer
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mRendererID);

    // put the data in the buffer
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(unsigned int), data, GL_STATIC_DRAW); // size in bytes

}

IndexBuffer::~IndexBuffer()
{
    glDeleteBuffers(1, &mRendererID);
}

void IndexBuffer::bind()
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mRendererID);
}

void IndexBuffer::unbind()
{
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
