/**
 * @file MainWindow.cpp
 * @author TSI, LJeannest
 * @brief Create the main window
 * @version 0.1
 * @date 2021-11-08
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : Link Controller to GlWidget
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * Modifs : add link to documentation file
 * VMathiot - 24/11/2021
 * Modifs : add logo and application name
 * VMathiot - 24/11/2021
 * Modifs : Handle the scale
 * Axelle Gaigé, Théo Huard - 2021-11-24
 * Modifs : add layer function : moveLayerUp/moveLayerDown/reMoveLayer/setVisibility
 *                               and the associated buttons.
 * Monge Maïlys, Rouyer Nicolas - 2021-11-26
 * Modifs : Change camera 2D to 3D
 * Amaryllis Vignaud, Axelle Gaigé - 2021-11-26
 * Modifs : Add particular window to choose attribute Hauteur
 * change getLayerinfo and showlayerX
 * Claire-Marie Alla - 2021-11-29
 * Modifs : fix docklayer function to handle WFS stream
 * Monge Maïlys, 2021-11-29

 */

#include <Glwidget.h>
#include <iostream>
#include <mainwindow.h>
#include <QBoxLayout>
#include <QComboBox>
#include <QDesktopServices>
#include <QDockWidget>
#include <QFileDialog>
#include <QIcon>
#include <QStyleOptionTitleBar>
#include <QListWidget>
#include <QMessageBox>
#include <QObject>
#include <QOpenGLWidget>
#include <QStyleOptionTitleBar>
#include <QWidget>
#include <ui_mainwindow.h>
#include "Shapefile.h"


MainWindow::MainWindow(Controller * controller, QWidget *parent): QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->controller = controller;
    ui->openGLWidget->setController(controller);

    dockImports = new DockImports();
    ui->dockWidget_Import->setWidget(dockImports);

    dockLayers = new DockLayers();
    ui->dockWidget_Layers->setWidget(dockLayers);

    //Raise the buttons above the openGL widget
    ui->pushButton2D->raise();
    ui->pushButton_zoomIn->raise();
    ui->pushButton_zoomOut->raise();



    // Whenever the openGL widget is resized, the buttons move.
    QObject::connect(ui->openGLWidget, SIGNAL(resized()),
                     this, SLOT(moveButtons()));

    //Open the SecDialog to import a file
    QObject::connect(ui->action_toolBarTop_ImportLayer,SIGNAL(triggered()),
            this,SLOT(showWindowsLayersVect()));
    QObject::connect(ui->action_Layer_AddLocalShp,SIGNAL(triggered()),
            this,SLOT(showWindowsLayersVect()));
    QObject::connect(ui->action_toolBarTop2_AddLocalShp,SIGNAL(triggered()),
            this,SLOT(showWindowsLayersVect()));
    QObject::connect(ui->action_Layer_AddLocalRaster,SIGNAL(triggered()),
            this,SLOT(showWindowsLayersRast()));
    QObject::connect(ui->action_toolBarTop2_AddLocalRaster,SIGNAL(triggered()),
            this,SLOT(showWindowsLayersRast()));
    QObject::connect(ui->action_toolBarTop2_AddFlow,SIGNAL(triggered()),
            this,SLOT(showWindowsLayersFlow()));
    QObject::connect(ui->action_Layer_AddFlow,SIGNAL(triggered()),
            this,SLOT(showWindowsLayersFlow()));
    QObject::connect(ui->action_toolBarTop_addCityGML,SIGNAL(triggered()),
            this,SLOT(showWindowsLayersCityGML()));
    QObject::connect(ui->actionAdd_a_layer_via_CityGML,SIGNAL(triggered()),
            this,SLOT(showWindowsLayersCityGML()));




    //Action for manage layer in dockLayers
    QAction::connect(dockLayers->getActionLayerUp(), SIGNAL(triggered(bool)),
                         this, SLOT(moveLayerUp()));
    QAction::connect(dockLayers->getActionLayerDown(), SIGNAL(triggered(bool)),
                         this, SLOT(moveLayerDown()));
    QAction::connect(dockLayers->getActionLayerDelete(), SIGNAL(triggered(bool)),
                         this, SLOT(removeLayer()));
    //event triggered when an item in the list is triggered and send this item
    QListWidget::connect(dockLayers->getListLayers(),SIGNAL(itemChanged(QListWidgetItem*)),
                         this, SLOT(setVisibility(QListWidgetItem*)));

    //Action for see the documentation
    QObject::connect(ui->actionView_Documentation, SIGNAL(triggered(bool)),
                     this, SLOT(viewDocumentation()));

    //Button + & - to zoom
    QObject::connect(ui->pushButton_zoomIn, SIGNAL(clicked()), this, SLOT(zoomIn()));
    QObject::connect(ui->pushButton_zoomOut, SIGNAL(clicked()), this, SLOT(zoomOut()));

    //Action when a layer from a stream is imported
       QObject::connect(dockImports->getListStream(),SIGNAL(itemDoubleClicked(QListWidgetItem *)),this,SLOT(loadVertexLayerFlow(QListWidgetItem*)));


    //Button in menu view to zoom
    QObject::connect(ui->action_View_ZoomIn, SIGNAL(triggered()), this, SLOT(zoomIn()));
    QObject::connect(ui->action_View_ZoomOut, SIGNAL(triggered()), this, SLOT(zoomOut()));

    //Button change of dimensions
    QObject::connect(ui->pushButton2D, SIGNAL(clicked()), this, SLOT(switchOverXD()));

    //Add background map in dockLayer
    string name = "Background Map";
    dockLayers->addLayer(QString::fromStdString(name));
}

MainWindow::~MainWindow(){
    delete ui;
}

int MainWindow::moveButtons(){

    ui->pushButton2D->move(ui->openGLWidget->width()+9-85,ui->openGLWidget->height()+9-125);

    ui->pushButton_zoomIn->move(ui->openGLWidget->width()+9-50,ui->openGLWidget->height()+9-70);

    ui->pushButton_zoomOut->move(ui->openGLWidget->width()+9-50,ui->openGLWidget->height()+9-40);

    ui->pushButton2D->raise();
    ui->pushButton_zoomIn->raise();
    ui->pushButton_zoomOut->raise();

    return 0;

}

void MainWindow::moveLayerUp(){
    //get QlistObject
    QWidget * dockWidget_Layers = qApp->focusWidget()->parentWidget()->parentWidget();


    // verify the QlistObject
    if (dockWidget_Layers->objectName().toStdString() == "dockWidget_Layers") {

        //move selected item one position below
        QListWidget* actualList = dockLayers->getListLayers();

        if(actualList->count()==0){
            return;
        }

        //move selected item one position below
        int currentRow = actualList->currentRow();

        //Check position is not 0
        if(currentRow==0){
            return;
        }

        //Get Name string
        string nameSelection = actualList->item(currentRow)->text().toStdString();
        string nameSwap = actualList->item(currentRow-1)->text().toStdString();

        //Move in the Model
        controller->moveItem(nameSelection,nameSwap);

        QListWidgetItem* currentItem = actualList->takeItem(currentRow);
        actualList->insertItem(currentRow - 1, currentItem);

        //refresh
        updateView();
        //Set Current row
        actualList->setCurrentRow(currentRow - 1);
    }
}

void MainWindow::moveLayerDown(){

    //get QlistObject
    QWidget * dockWidget_Layers = qApp->focusWidget()->parentWidget()->parentWidget();

    // verify the QlistObject
    if (dockWidget_Layers->objectName().toStdString() == "dockWidget_Layers") {

        //move selected item one position above
        QListWidget* actualList = dockLayers->getListLayers();
        if(actualList->count()==0){
            return;
        }

        //move selected item one position above

        int currentRow = actualList->currentRow();
        //Check position is not the end of the list
        if(currentRow==actualList->count()-1){
            return;
        }
        //Get Name string
        string nameSelection = actualList->item(currentRow)->text().toStdString();
        string nameSwap = actualList->item(currentRow+1)->text().toStdString();
        //Move in the Model
        controller->moveItem(nameSelection,nameSwap);

        QListWidgetItem* currentItem = actualList->takeItem(currentRow);
        actualList->insertItem(currentRow + 1, currentItem);
        //refresh
        updateView();
        //Set Current row
        actualList->setCurrentRow(currentRow + 1);
    }
}

void MainWindow::removeLayer(){
    //get QlistObject
    QWidget * dockWidget_Layers = qApp->focusWidget()->parentWidget()->parentWidget();

    // verify the QlistObject
    if (dockWidget_Layers->objectName().toStdString() == "dockWidget_Layers") {
        //get list
        QListWidget* actualList = dockLayers->getListLayers();
        if(actualList->count()==0){
            return;
        }

        const QString& itemSelected = dockLayers->getListLayers()->currentItem()->text();

        //remove selected layer by this name
        controller->removeLayer(itemSelected.toStdString());

        //remove this item from the list
        int currentRow = actualList->currentRow();
        QListWidgetItem* currentItem = actualList->takeItem(currentRow);
        actualList->removeItemWidget(currentItem);


        //refresh
        updateView();
    }
}

void MainWindow::setVisibility(QListWidgetItem *itemChanged) {
    //get name of QListItem who as changed
    string itemChangedName = itemChanged->text().toStdString();

    //check if is unchecked or not and set visibility of this layer
    if(itemChanged->checkState() == Qt::Unchecked){
        this->controller->setVisibility(itemChangedName,false);
    }
    else{
        this->controller->setVisibility(itemChangedName,true);
    }

    //refresh the view
    ui->openGLWidget->update();
}

void MainWindow::zoomIn(){;
    ui->openGLWidget->zoom(-1);
    updateScale();
}

void MainWindow::zoomOut(){

    ui->openGLWidget->zoom(1);
    updateScale();
}

void MainWindow::resizeEvent(QResizeEvent*)
{
    //If it's not the first time window is resized update scale
    if (not firstTimeResize){

        updateScale();
    }

    //If it's the first time window is resized do nothing
    else {

        firstTimeResize = false;
    }
}

void MainWindow::wheelEvent(QWheelEvent *event){
    updateScale();
}

void MainWindow::updateScale(){
    string scale = ui->openGLWidget->calculateScale(); //Calculate new scale
    ui->action_toolBarBottom_scale->setText(scale.c_str()); //Update text with new scale
}

void MainWindow::viewDocumentation(){
    QString path = "../doc/documentation.html";
    QDesktopServices::openUrl(path);

}

void MainWindow::showWindowsLayersVect()
{
    QString tab ("Vector");
    sDialog = new SecDialog(this, tab) ;
    sDialog->show();

    //Get the file's informations when the window is closed
    QObject::connect(sDialog, SIGNAL(finished(int)),
             this, SLOT(showParticularWindowsLayers()));
}

void MainWindow::switchOverXD(){

    controller->setSwithOverXD();

    if (controller->getDisplay3D()){

        ui->pushButton2D->setText("3D");
        ui->openGLWidget->switch3D();
    }

    else{

        ui->pushButton2D->setText("2D");
        ui->openGLWidget->switch2D();
    }
    updateView();
}

void MainWindow::showWindowsLayersRast()
{

    QString tab ("Raster");
    sDialog = new SecDialog(this, tab) ;
    sDialog->show();

    //Get the file's informations when the window is closed
    QObject::connect(sDialog, SIGNAL(finished(int)),
            this, SLOT(updateLayer()));
}

void MainWindow::showWindowsLayersFlow()
{

    QString tab ("Flow");
    sDialog = new SecDialog(this, tab) ;
    sDialog->show();

    //Get the file's informations when the window is closed
    QObject::connect(sDialog, SIGNAL(finished(int)),
            this, SLOT(updateLayer()));

}


void MainWindow::showWindowsLayersCityGML()
{

    QString tab ("CityGML");
    sDialog = new SecDialog(this, tab) ;
    sDialog->show();

    //Get the file's informations when the window is closed
    QObject::connect(sDialog, SIGNAL(finished(int)),
            this, SLOT(updateLayer()));
}

bool MainWindow::getLayerInfo(){
    QString layerType = sDialog->getType();
    QString layerName = sDialog->getName();
    QString layerPath = sDialog->getPath();


    //Name checkout
    string shortName = controller->checkName(layerName.toStdString());
    QString qstrfilename = QString::fromStdString(shortName);

    if (layerType == "Vector")
    {
        std::cout << "Loading Vector Layer" << std::endl;

        if (!layerPath.isEmpty()){

            QByteArray ba = layerPath.toLocal8Bit();
            string filePath = ba.data();
            string fileName = filePath.substr(filePath.find_last_of("/\\")+1);
            string extent = fileName.substr(fileName.find_last_of("/."));

            bool goodExtent = this->controller->verifyExtent(filePath,extent);
            if (goodExtent){

                int loadResponse = this->controller->loadShp(filePath,shortName);
                if (loadResponse == 1){

                    QMessageBox AlertWrongExtent;
                    AlertWrongExtent.setText("You are loading a non-existent file");
                    AlertWrongExtent.exec();
                    return false;
                }else if(loadResponse == 2){

                    QMessageBox AlertWrongExtent;
                    AlertWrongExtent.setText("Your file can't be loaded");
                    AlertWrongExtent.exec();
                    return false;
                }
                else{
                    dockLayers->addLayer(qstrfilename);

                    std::cout << "Vector Layer Loaded" << std::endl;
                    return true;
                }
            }else{

                QMessageBox AlertWrongExtent;
                AlertWrongExtent.setText("You are not loading the expected data type");
                AlertWrongExtent.exec();
                return false;
            }
        }

        dockLayers->addLayer(qstrfilename);

        std::cout << "Vector Layer Loaded" << std::endl;
    }


    else if (layerType == "Raster")
    {

        std::cout << "Loading Raster Layer" << std::endl;

        if(!layerPath.isEmpty()){

            QByteArray ba = layerPath.toLocal8Bit();
            string filePath = ba.data();
            string fileName = filePath.substr(filePath.find_last_of("/\\")+1);
            string extent = fileName.substr(fileName.find_last_of("/."));
            bool goodExtent = this->controller->verifyExtent(filePath,extent);

            if (goodExtent){
                int loadResponse = this->controller->loadTif(filePath,shortName);
                if (loadResponse == 1){

                    QMessageBox AlertWrongExtent;
                    AlertWrongExtent.setText("You are loading a non-existent file");
                    AlertWrongExtent.exec();
                    return false;
                }else if(loadResponse == 2){

                    QMessageBox AlertWrongExtent;
                    AlertWrongExtent.setText("Your file can't be loaded");
                    AlertWrongExtent.exec();
                    return false;
                }
                else {
                    dockLayers->addLayer(qstrfilename);

                    std::cout << "Raster Layer Loaded" << std::endl;
                    return true;
                }
            }else{

                QMessageBox AlertWrongExtent;
                AlertWrongExtent.setText("You are not loading the expected data type");
                AlertWrongExtent.exec();
                return false;
            }
        }
        dockLayers->addLayer(qstrfilename);

        dockLayers->addLayer(qstrfilename);

        std::cout << "Raster Layer Loaded" << std::endl;
        return 0;
    }


    else if (layerType == "CityGML")
    {
        std::cout << "Loading CityGML Layer" << std::endl;

        std::cout << "Name : " << layerName.toStdString() << std::endl;
        std::cout << "Path : " << layerPath.toStdString() << std::endl;

        std::cout << "CityGML Layer Loaded" << std::endl;
        return false;
    }


    else if (layerType == "WFS")
    {
        std::cout << "Loading Layer via WFS" << std::endl;

        std::cout << "Name : " << layerName.toStdString() << std::endl;
        std::cout << "URL : " << layerPath.toStdString() << std::endl;

        loadFlow(layerPath.toStdString(),layerName.toStdString());


        std::cout << "WFS Layer Loaded" << std::endl;
        return false;
    }


    else if (layerType == "WMS")
    {
        std::cout << "Loading Layer via WMS" << std::endl;

        std::cout << "Name : " << layerName.toStdString() << std::endl;
        std::cout << "URL : " << layerPath.toStdString() << std::endl;

        std::cout << "WMS Layer Loaded" << std::endl;
        return false;
    }


    else
    {
        std::cout << "Layer loading cancelled" << std::endl;
        return false;
    }
    return false;
}

void MainWindow::showParticularWindowsLayers()
{
    // Load shp
    updateLayer();
    SecParticularDialog *secPDialog = new SecParticularDialog(controller);
    // Open little window
    secPDialog->show();

    QObject::connect(secPDialog, SIGNAL(finished(int)),
                     this, SLOT(updateView()));
}

void MainWindow::updateLayer()
{
    if(getLayerInfo())
        ui->openGLWidget->update();
}

void MainWindow::updateView()
{
    //refresh
    ui->openGLWidget->update();
}

//Functions for listening Stream Data

void MainWindow::loadFlow(string path, string name){

    vector<string> allNames;
    controller->loadStream(path,name);
    allNames = controller->getLayerNameStream(path);
    for(int i=0; i<allNames.size();i++){
        QString qname =  QString::fromStdString(allNames[i]);
        dockImports->addWFS(qname);
    }
}

void MainWindow::loadVertexLayerFlow(QListWidgetItem* item){
    string nameSelected = item->text().toStdString();
    //std::cout<<"bonjour"<<std::endl;
    controller->loadVertexLayerStream(nameSelected);
    //Add layer on the docklayer
    dockLayers->addLayer(QString::fromStdString(nameSelected));
    //refresh
    ui->openGLWidget->update();
    std::cout<<"[SUCCESS] The Stream has been loaded"<<std::endl;



}

