/*! \class DockImport
    \brief Create the dock that will manage the imports
    \author LJeannest
    \date 19/11/2021
    \details
    \version 1.0

 *  Modifs : function addWFS()
 *  Monge Maïlys 2021-11-29

 */

#include "DockImports.h"

#include <QBoxLayout>
#include <QListWidget>
#include <QToolBar>
#include <QWidget>
#include <QFileSystemModel>

using namespace std;


DockImports::DockImports() : QWidget(){

    toolBarImports = new QToolBar();
    toolBarImports->setLayoutDirection(Qt::LeftToRight);

    //create the actions to put in the toolBar
    action_Import_add = new QAction();
    QPixmap add (":/icons/import/png_icons_sig/Imports/ajouter.png");
    action_Import_add->setIcon(add);
    action_Import_add->setToolTip("Add");


    action_Import_search = new QAction();
    QPixmap search (":/icons/import/png_icons_sig/Imports/chercher.png");
    action_Import_search->setIcon(search);
    action_Import_search->setToolTip("Search");

    action_Import_shrink = new QAction();
    QPixmap shrink (":/icons/import/png_icons_sig/Imports/retracter.png");
    action_Import_shrink->setIcon(shrink);
    action_Import_shrink->setToolTip("Shrink all files");

    action_Import_refresh = new QAction();
    QPixmap refresh (":/icons/import/png_icons_sig/Imports/actualiser.png");
    action_Import_refresh->setIcon(refresh);
    action_Import_refresh->setToolTip("Refresh");

    /*
    action_Import_Layer_Created = new QAction();
    QPixmap download (":/icons/import/png_icons_sig/Imports/Téléchargement.png");
    action_Import_Layer_Created->setIcon(download);
    action_Import_Layer_Created->setToolTip("Download_layers");
    */


    //add buttons to the toolbar
    toolBarImports->addAction(action_Import_add);
    toolBarImports->addAction(action_Import_search);
    toolBarImports->addAction(action_Import_shrink);
    toolBarImports->addAction(action_Import_refresh);
    //toolBarImports->addAction(action_Import_Layer_Created);

    //Set layer view for file
    layoutImport = new QVBoxLayout();
    treeView = new QTreeView();



    QString sPath = "F:/";
    dirmodel = new QFileSystemModel(this);
    dirmodel->setFilter(QDir::NoDotAndDotDot | QDir::AllDirs | QDir::Files);
    dirmodel->setRootPath(sPath);
    treeView->setModel(dirmodel);

    layoutImport->addWidget(toolBarImports);
    layoutImport->addWidget(treeView);

    this->setLayout(layoutImport);

    //Set layer view for WFS stream
    listStream = new QListWidget();
    layoutImport->addWidget(listStream);

}

DockImports::~DockImports(){

}

void DockImports::addWFS(QString layerName){
    //create a new item in the list widget with the name of layer
    QListWidgetItem *newLayer = new QListWidgetItem;
    newLayer->setText(layerName);
    //insert item
    listStream->addItem(newLayer);
}


