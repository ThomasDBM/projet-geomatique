/* Renderer.cpp
 * Render the elements
 * Claire-Marie Alla
 * Adaption to Qt
 * Charzat Maxime
 * First merge : 23/11/2021
*/

#include <Renderer.h>

void Renderer::initializeRenderer()
{
    initializeOpenGLFunctions();
}

void Renderer::clear()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::draw(GLenum geom_type, VertexArray& va, IndexBuffer& ib, Shader& shader)
{
    shader.bind();
    va.bind();
    ib.bind();

    glDrawElements(geom_type, ib.getCount(), GL_UNSIGNED_INT, nullptr);

    shader.unBind();
    va.unbind();
    ib.unbind();
}
