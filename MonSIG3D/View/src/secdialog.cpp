/*! \class secdialog
    \brief Create windows to select layer
    \author AZebaze
    \date 23/11/2021
    \details
    \version 1.0
 */


#include <secdialog.h>
#include <ui_secdialog.h>
#include <iostream>
#include <QFileDialog>
#include <QPixmap>
#include <QPushButton>

#include <QToolButton>
#include <QWidget>

SecDialog::SecDialog(QWidget *parent, QString tab) :
    QDialog(parent),
    ui(new Ui::SecDialog)
{

    ui->setupUi(this);
    setFixedSize(600,400);

    if (tab=="Flow"){
        ui->tabWidget->setCurrentWidget(ui->tabWidgetWFS);
    }
    else if (tab=="Raster"){
        ui->tabWidget->setCurrentWidget(ui->tabWidgetRaster);
    }
    else if (tab=="CityGML"){
        ui->tabWidget->setCurrentWidget(ui->tabWidgetCityGML);
    }
    else {
        ui->tabWidget->setCurrentWidget(ui->tabWidgetVector);
    }

    //Open a QFileDialog when the button Search is clicked
    connect(ui->pushButton_Vector_Search, SIGNAL(clicked(bool)), this, SLOT(openFileDialogVector()));
    //Close the window when the button Cancel is clicked
    connect(ui->pushButton_Vector_Cancel, SIGNAL(clicked(bool)), this, SLOT(cancel()));
    //Close the window an save the vector layer
    connect(ui->pushButton_Vector_OK, SIGNAL(clicked(bool)), this, SLOT(addVectorLayer()));

    //Open a QFileDialog when the button Search is clicked
    connect(ui->pushButton_Raster_Search, SIGNAL(clicked(bool)), this, SLOT(openFileDialogRaster()));
    //Close the window when the button Cancel is clicked
    connect(ui->pushButton_Raster_Cancel, SIGNAL(clicked(bool)), this, SLOT(cancel()));
    //Close the window an save the raster layer
    connect(ui->pushButton_Raster_OK, SIGNAL(clicked(bool)), this, SLOT(addRasterLayer()));

    //Open a QFileDialog when the button Search is clicked
    connect(ui->pushButton_CityGML_Search, SIGNAL(clicked(bool)), this, SLOT(openFileDialogCityGML()));
    //Close the window when the button Cancel is clicked
    connect(ui->pushButton_CityGML_Cancel, SIGNAL(clicked(bool)), this, SLOT(cancel()));
    //Close the window an save the cityGML layer
    connect(ui->pushButton_CityGML_OK, SIGNAL(clicked(bool)), this, SLOT(addCityGMLLayer()));

    //Close the window when the button Cancel is clicked
    connect(ui->pushButton_Flow_Cancel, SIGNAL(clicked(bool)), this, SLOT(cancel()));
    //Close the window an save the WFS layer
    connect(ui->pushButton_Flow_OK, SIGNAL(clicked(bool)), this, SLOT(addFlowLayer()));

}

SecDialog::~SecDialog()
{
    delete ui;
}

void SecDialog::openFileDialogVector(){
    QString pathStr = QFileDialog::getOpenFileName(this,
                                                    "Select one or more files to open",
                                                    "/home",
                                                    "Shapefile (*.shp)");

    ui->lineEdit_Vector_ChooseFile->setText(pathStr);
    //Get ride of the extent
    std::string fileName=pathStr.mid(pathStr.lastIndexOf('/')+1).toStdString();
    size_t pos = fileName.std::string::find(".");
    std::string shortName = fileName.std::string::substr(0,pos);

    ui->lineEdit_Vector_Name->setText(QString::fromStdString(shortName));
}


void SecDialog::openFileDialogRaster(){
    QString pathStr = QFileDialog::getOpenFileName(this,
                                                    "Select one or more files to open",
                                                    "/home",
                                                    "Images (*.tif *.tiff)");

    ui->lineEdit_Raster_ChooseFile->setText(pathStr);
    //get ride of the extent
    std::string fileName=pathStr.mid(pathStr.lastIndexOf('/')+1).toStdString();
    size_t pos = fileName.std::string::find(".");
    std::string shortName = fileName.std::string::substr(0,pos);

    ui->lineEdit_Raster_Name->setText(QString::fromStdString(shortName));
}

void SecDialog::openFileDialogCityGML(){
    QString pathStr = QFileDialog::getOpenFileName(this,
                                                    "Select one or more files to open",
                                                    "/home",
                                                    "cityGML (*.md)");

    ui->lineEdit_CityGML_ChooseFile->setText(pathStr);

    ui->lineEdit_CityGML_Name->setText(pathStr.mid(pathStr.lastIndexOf('/')+1));
}


void SecDialog::cancel(){
    this->close();
    lastLayerType="";
    lastLayerName="";
    lastLayerPath="";
}

void SecDialog::addVectorLayer(){
    lastLayerType="Vector";
    lastLayerName=ui->lineEdit_Vector_Name->text();
    lastLayerPath=ui->lineEdit_Vector_ChooseFile->text();
    this->close();
}

void SecDialog::addRasterLayer(){
    lastLayerType="Raster";
    lastLayerName=ui->lineEdit_Raster_Name->text();
    lastLayerPath=ui->lineEdit_Raster_ChooseFile->text();
    this->close();
}

void SecDialog::addCityGMLLayer(){
    lastLayerType="CityGML";
    lastLayerName=ui->lineEdit_CityGML_Name->text();
    lastLayerPath=ui->lineEdit_CityGML_ChooseFile->text();
    this->close();
}

void SecDialog::addFlowLayer(){
    if (ui->radioButton_Flow_WFS->isChecked())
    {
        lastLayerType="WFS";
    }
    else if (ui->radioButton_Flow_WMS->isChecked())
    {
        lastLayerType="WMS";
    }
    lastLayerName=ui->lineEdit_Flow_Name->text();
    lastLayerPath=ui->lineEdit_Flow_URL->text();
    this->close();
}



QString SecDialog::getType(){
    return lastLayerType;
}

QString SecDialog::getName(){
    return lastLayerName;
}

QString SecDialog::getPath(){
    return lastLayerPath;
}
