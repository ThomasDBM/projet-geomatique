/* VertexBuffer.cpp
 * Describe and manage the vertex buffer
 * Claire-Marie Alla
 * Adaption to Qt
 * Charzat Maxime
 * First merge : 23/11/2021
*/


#include <VertexBuffer.h>


VertexBuffer::VertexBuffer(const void* data, unsigned int size)
{
    initializeOpenGLFunctions();
    glGenBuffers(1, &mRendererID); // 1 for 1 buffer
    glBindBuffer(GL_ARRAY_BUFFER, mRendererID);
    // Mettre les donnees dedans
    glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW); // size in bytes

}

VertexBuffer::~VertexBuffer()
{
    glDeleteBuffers(1, &mRendererID);
}

void VertexBuffer::bind()
{
    glBindBuffer(GL_ARRAY_BUFFER, mRendererID);
}

void VertexBuffer::unbind()
{
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}
