/* VertexBuffer.cpp
 * Describe and manage the vertex buffer layout
 * Claire-Marie Alla
 * Adaption to Qt
 * Charzat Maxime
 * First merge : 23/11/2021
*/

#include <VertexBufferLayout.h>


VertexBufferLayout::VertexBufferLayout() : mStride(0) {

    initializeOpenGLFunctions();
}

VertexBufferLayout::~VertexBufferLayout(){

}

void VertexBufferLayout::push(unsigned int count)
{
    mElements.push_back({ GL_FLOAT, count, GL_FALSE });
    mStride += count * VertexBufferElement::getSizeOfType(GL_FLOAT); // 4 = sizeof(float) or sizeof(GLfloat)
}
