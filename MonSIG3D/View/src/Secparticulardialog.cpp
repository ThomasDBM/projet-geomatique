/**
 * @file SecParticularDialog
 * @author AZebaze, Claire-Marie Alla
 * @brief Create the window to select 2.5D field
 * @version 1.0
 * @date 2021-11-25
 *
 *
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : Add combo field and getfieldHauteur
 * AZebaze, Claire-Marie Alla - 2021-11-29
**/

#include "Secparticulardialog.h"
#include "ui_Secparticulardialog.h"
#include "mainwindow.h"
#include "Controller.h"
#include <QComboBox>
#include <QDesktopServices>
#include <QFileDialog>
#include <QIcon>
#include <QOpenGLWidget>
#include <QPixmap>
#include <vector>
#include <QSize>
#include <string>

SecParticularDialog::SecParticularDialog(Controller * controller, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SecParticularDialog),
    controller(controller){

    ui->setupUi(this);

    comboField();

    connect(ui->pushButton_OK, SIGNAL(clicked(bool)), this, SLOT(getFieldHauteur()));
}

SecParticularDialog::~SecParticularDialog(){
    delete ui;
}

void SecParticularDialog::comboField(){

    // Retrieve list of shp attributes
    vector<string> fields = this->controller->getFields();

    QStringList list;
    for(int i=0; i < fields.size(); i++){
        list << QString::fromStdString(fields[i]);
    }

    ui->comboBox->addItem("No height");
    ui->comboBox->addItems(list);
}


void SecParticularDialog::getFieldHauteur(){

    QString s = ui->comboBox->currentText();
    // Modify the argument fieldHauteur in the class
    this->controller->addFieldHauteur(s.toUtf8().constData());
    // Close the window
    close();
}


