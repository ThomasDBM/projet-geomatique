/* VertexArray.cpp
 * Describe and manage the vertex array
 * Claire-Marie Alla
 * Adaption to Qt
 * Charzat Maxime
 * First merge : 23/11/2021
*/

#include <VertexBufferLayout.h>
#include <VertexArray.h>

VertexArray::VertexArray()
{
}

VertexArray::~VertexArray()
{
    glDeleteVertexArrays(1, &mRendererID);
}

void VertexArray::initializeVertexArray()
{
    initializeOpenGLFunctions();
    glGenVertexArrays(1, &mRendererID);
}

void VertexArray::addBuffer(VertexBuffer& vb, const VertexBufferLayout& layout)
{
    bind(); // bind the vertex array
    vb.bind(); // bind the buffer
    const auto& elements = layout.getElements();
    unsigned  int offset = 0;

    for (unsigned int i = 0; i < elements.size(); i++)
    {
        const auto& element = elements[i];
        glEnableVertexAttribArray(i);
        glVertexAttribPointer(i, element.count, element.type,
            element.normalized, layout.getStride(), (const void*) offset);

        // first 0 is the index of my attribut => same location as below
        offset += element.count * VertexBufferElement::getSizeOfType(element.type);
    }
}

void VertexArray::bind()
{
    glBindVertexArray(mRendererID);
}

void VertexArray::unbind()
{
    glBindVertexArray(0);
}
