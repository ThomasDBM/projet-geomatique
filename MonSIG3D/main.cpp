/* main.cpp
 * main program
 * TSI
 * First merge : 04/11/2021
 * Modifs : Add lines to allow display in VM
 * Axelle Gaigé - 29/11/2021
*/

#include <iostream>


#include "mainwindow.h"

#include <Shapefile.h>

#include <QApplication>
#include <QSurfaceFormat>
#include "DockLayers.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QSurfaceFormat fmt;
    fmt.setVersion(3, 3);
    fmt.setProfile( QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat( fmt );

    // Create model
    Model * m = new Model();

    // Create Controller
    Controller * c = new Controller(m);

    // Create View
    MainWindow w(c);

    //Map
    string shpPath = "../MonSIG3D/data/Shapefile/adr_voie_lieu/adr_voie_lieu.adrarrond.shp";
    string name = "Background Map";
    string type = "shapefile";

    c->loadShp(shpPath,name);
    c->addFieldHauteur("No height");

    w.show();
    return a.exec();

}
