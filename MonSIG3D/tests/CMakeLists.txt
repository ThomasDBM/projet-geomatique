cmake_minimum_required(VERSION 3.5)

set(BINARY test_SIG)

file(GLOB_RECURSE TEST_SOURCES LIST_DIRECTORIES false *.h *.cpp)

include_directories(${GTEST_INCLUDE_DIR})
set(SOURCES ${TEST_SOURCES})

add_executable(test_SIG ${TEST_SOURCES})

target_link_libraries(test_SIG PUBLIC ${CMAKE_PROJECT_NAME}_lib GTest::GTest GTest::Main)
target_link_libraries(test_SIG PUBLIC Qt5::Widgets Qt5::Core OpenGL::GL gdal glfw GLEW)
