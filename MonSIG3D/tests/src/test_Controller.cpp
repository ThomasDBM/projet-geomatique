/**
 * @file test_Controller.cpp
 * @author Paul Miancien
 * @brief testing methods in the controller class 
 * @version 0.1
 * @date 2021-11-23
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : Add tests for getVertexToPaint
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * @modif Theo Herman, add tests 24/11/21
 * Modifs : change the geotiff file used in tests
 * Maxime Charzat - 2021-12-01
 */

#include <Controller.h>
#include <gtest/gtest.h>
#include <Model.h>
#include <Shapefile.h>

using namespace std;

TEST(controller, verifyExtent) {
    //arrange
    Model model;
    string test_string = "../../MonSIG3D/data/Shapefile/polygone/polygone.shp";
    Controller control(&model);

    //act


    //assert
    EXPECT_TRUE(control.verifyExtent(test_string,"shp"));
}

TEST(controller, getVertex) {
    //arrange
    Model * m = new Model();
    m->addLayer("shapefile", "shp_test", "../../MonSIG3D/data/Shapefile/polygone/polygone.shp");

    Controller * c = new Controller(m);

    //act
    vector<Data*> layers = c->getLayersToPaint();

    //assert
    EXPECT_TRUE(layers.size() == 1);
    EXPECT_TRUE(layers[0]->getName() == "shp_test");
}

TEST(controller, getVertexVisibility) {
    //arrange
    Model * m = new Model();
    m->addLayer("shapefile", "shp_test", "../../MonSIG3D/data/Shapefile/polygone/polygone.shp");
    m->getLayerByPosition(0)->setVisibility(false);

    Controller * c = new Controller(m);

    //act
    vector<Data*> layers = c->getLayersToPaint();

    //assert
    EXPECT_TRUE(layers.size() == 0);
}

TEST(controller, loadShpGood) {
    //arrange
    Model model;
    string test_string = "../../MonSIG3D/data/Shapefile/polygone/polygone.shp";
    Controller control(&model);

    //act
    control.loadShp(test_string,"name_test");

    //assert
    EXPECT_FALSE(control.model->layers.empty());
}

TEST(controller, loadShpBad) {
    //arrange
    Model model;
    string test_string = "isnot.shp";
    Controller control(&model);

    //act
    control.loadShp(test_string,"name_test");

    //assert
    EXPECT_TRUE(control.model->layers.empty());
}


TEST(controller, loadTifGood) {
    //arrange
    Model model;
    string test_string = "../../MonSIG3D/data/GeoTIFF/Lyon_arr5_petit/ortho_arr5_resize.tif";
    Controller control(&model);

    //act
    control.loadTif(test_string,"name_test", "../../MonSIG3D/data/GeoTIFF/Lyon_arr5/mnt_arr5.tif");

    //assert
    EXPECT_FALSE(control.model->layers.empty());
}

TEST(controller, loadTifBad) {
    //arrange
    Model model;
    string test_string = "isnot.tif";
    Controller control(&model);

    //act
    control.loadTif(test_string,"name_test", "../../MonSIG3D/data/GeoTIFF/Lyon_arr5/mnt_arr5.tif");

    //assert
    EXPECT_TRUE(control.model->layers.empty());
}

TEST(controller, addFieldHauteur) {
    //arrange
    Model model;
    string test_string = "../../MonSIG3D/data/Shapefile/polygone/batiments_arr5.shp";
    Controller control(&model);

    //act
    control.loadShp(test_string,"name_test");
    control.addFieldHauteur("HAUTEUR");
    int position = control.model->getNumberOfLayers()-1;
    Data * data = control.model->getLayerByPosition(position);

    //assert
    EXPECT_TRUE(data->getExtrude());
    EXPECT_TRUE(data->getField3D() != "No height");
}
