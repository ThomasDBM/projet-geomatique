/**
 * @file test_Model.cpp
 * @author Melodia Mohad
 * @brief
 * @version 0.1
 * @date 2021-11-17
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : tests for addLayer function
 * Amaryllis Vignaud, Maxime Charzat - 2021-11-24
 * Modifs : change the geotiff file used in tests
 * Maxime Charzat - 2021-12-01
 */

#include <gtest/gtest.h>
#include <Model.h>

using namespace std;

TEST(model, addLayerGeotiff) {
    //arrange
    string path = "../../MonSIG3D/data/GeoTIFF/Lyon_arr5_petit/ortho_arr5_resize.tif";
    string name = "Fond de carte";

    //act
    Model model;

    //assert
    EXPECT_EQ(model.addLayer("geotiff" , name, path, "../../MonSIG3D/data/GeoTIFF/Lyon_arr5/mnt_arr5.tif"),0);
    EXPECT_EQ(1 , model.getNumberOfLayers());
}

TEST(model, addLayerShapefile) {
    //arrange
    string path = "../../MonSIG3D/data/Shapefile/points/deux_points.shp";
    string name = "Deux points";

    //act
    Model model;

    //assert
    EXPECT_EQ(model.addLayer("shapefile" , name, path),0);
    EXPECT_EQ(1 , model.getNumberOfLayers());
}

TEST(model, wrongAddLayerShapefile) {
    //arrange
    string path = "../../MonSIG3D/data/Shapefile/points/deux_points.dbf";
    string name = "Wrong File";

    //act
    Model model;

    //assert
    EXPECT_EQ(model.addLayer("shapefile" , name, path),1);
}

TEST(model, twoAddLayer) {
    //arrange
    string path = "../../MonSIG3D/data/Shapefile/points/deux_points.shp";
    string name = "Two points";
    string path2 = "../../MonSIG3D/data/GeoTIFF/Lyon_arr5_petit/ortho_arr5_resize.tif";
    string name2 = "Fond de carte";

    //act
    Model model;
    model.addLayer("shapefile" , name, path);
    model.addLayer("geotiff" , name2, path2, "../../MonSIG3D/data/GeoTIFF/Lyon_arr5/mnt_arr5.tif");
    //assert
    EXPECT_EQ(2 , model.getNumberOfLayers());
}
