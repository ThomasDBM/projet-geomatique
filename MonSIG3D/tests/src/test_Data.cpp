/**
 * @file test_Data.cpp
 * @author Melodia Mohad
 * @brief
 * @version 0.1
 * @date 2021-11-17
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : Add tests for color
 * Maxime Charzat - 2021-11-24
 */
#include <iostream>
#include <gtest/gtest.h>
#include <WMS.h>
#include <Shapefile.h>

using namespace std;

TEST(data,constructor) {
    //arrange
    string path = "my/path/data";
    string name = "datio";

    //act
    WMS datio(name,path);
    WMS datioDeux(name,path);

    //assert
    EXPECT_EQ(path,datio.path);
    EXPECT_EQ(name,datio.name);
    EXPECT_NE(NULL,datioDeux.id);
    EXPECT_NE(datio.id,datioDeux.id);
    EXPECT_EQ(true, datio.isVisible);
}

TEST(data, destructor) {
    //arrange

    //act

    //assert
    EXPECT_TRUE (true);
}

TEST(data, getId){
    //arrange
    string path = "path";
    string idid = "id";
    WMS datID(path,idid);

    //act
    int id = datID.getId();

    //assert
    EXPECT_EQ(datID.id,id);

}

TEST(data,getName){
    //arrange
    string name = "name";
    string path = "path";
    WMS datName(name, path);

    //act
    string nameGet = datName.getName();

    //assert
    EXPECT_EQ(datName.name,nameGet);
}

TEST(data,setName){
    //arrange
    string name = "name";
    string path = "path";
    WMS datSetName(name, path);
    string newName = "newName";

    //act
    datSetName.setName(newName);

    //assert
    EXPECT_EQ(newName,datSetName.name);
}

TEST(data,getType){
    //arrange
    string name = "name";
    string path = "path";
    WMS datGetType(name, path);

    //act
    string type = datGetType.getType();

    //assert
    EXPECT_EQ(datGetType.type,type);

}

TEST(data, setType){
    //arrange
    string name = "name";
    string path = "path";
    WMS datSetType(name, path);
    string newType = "newType";

    //act
    datSetType.setType(newType);

    //assert
    EXPECT_EQ(newType,datSetType.type);

}

TEST(data, getPath){
    //arrange
    string name = "name";
    string path = "path";
    WMS datGetPath(name, path);

    //act
    string pathGet = datGetPath.getPath();

    //assert
    EXPECT_EQ(datGetPath.path, pathGet);

}

TEST(data, setPath){
    //arrange
    string name = "name";
    string path = "path";
    WMS datSetPath(name, path);
    string newPath = "newPath";

    //act
    datSetPath.setPath(newPath);

    //assert
    EXPECT_EQ(newPath, datSetPath.path);

}

TEST(data, getVisibility){
    //arrange
    string name = "name";
    string path = "path";
    WMS datGetVisibility(name, path);

    //act
    bool visibilityGet = datGetVisibility.getVisibility();

    //assert
    EXPECT_EQ(datGetVisibility.isVisible, visibilityGet);
}

TEST(data, setVisibility){
    //arrange
    string name = "name";
    string path = "path";
    WMS datSetVisibility(name, path);
    bool newVisibility = false;

    //act
    datSetVisibility.setVisibility(newVisibility);

    //assert
    EXPECT_EQ(newVisibility, datSetVisibility.isVisible);
}

TEST(data, getGdalDataset){
    //arrange
    string name = "name";
    string path = "path";
    WMS datGetDataset(name, path);

    //act
    GDALDataset* datasetGet = datGetDataset.getGdalDataset();

    //assert
    EXPECT_EQ(datGetDataset.getGdalDataset(), datasetGet);
}

TEST(data, setGdalDataset){
    //arrange
    string name = "name";
    string path = "path";
    WMS datGetDataset(name, path);
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/testProj/regions-20180101.shp");

    //act
    datGetDataset.setGdalDataset(shape.getGdalDataset());

    //assert
    EXPECT_EQ(datGetDataset.getGdalDataset(), shape.getGdalDataset());
}

TEST(data, color){
    //arrange
    string name = "name";
    string path = "path";

    //act
    WMS data1(name, path);
    WMS data2(name, path);

    //assert
    EXPECT_TRUE(data1.getColor() != data2.getColor());

}
