/**
 * @file Test_Wfs.cpp
 * @author MMohad
 * @brief Serie of test
 * @version 0.1
 * @date 2021-11-10
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <iostream>
#include "gtest/gtest.h"
#include "Wfs.h"


TEST(wfs,constructor) {
    //arrange
    std::string wfsLink ="https://download.data.grandlyon.com/wms/grandlyon?SERVICE=WfS&REQUEST=GetCapabilities&VERSION=1.3.0";
    std::string name = "test_wfs";
    string layerName = "ms:adr_voie_lieu.adrarrond";

    //act
    Wfs wfsTest(name,wfsLink,layerName);
    Wfs wfsTestDeux(name,wfsLink,layerName);


    //assert
    EXPECT_EQ (wfsLink,wfsTest.path);
    EXPECT_EQ (name,wfsTest.name);
    EXPECT_EQ ("Wfs",wfsTest.type);
    EXPECT_NE (wfsTest.id,wfsTestDeux.id);

}

TEST(wfs, destructor) {
    //arrange


    //act

    //assert
    EXPECT_TRUE (true);
}

