/* test_Shapefile.cpp
 * All units tests
 * Claire-Marie Alla, Axelle Gaigé, Maxime Charzat
 * First merge : 10/11/2021
 * Modifs : Add 2 tests
 * Amaryllis Vignaud - 15/11/2021
 * Modifs : Add tests for vertex
 * Axelle Gaigé, Maxime Charzat - 17/11/2021
 * Modifs : Refactor functions to combine the generation of 2D and 3D vertex
 * Charzat Maxime - 17/11/2021
 * Modifs : Add reprojection test
 * Claire-Marie Alla, Amaryllis Vignaud - 17/11/2021
 * Modifs : Fix missing tests and correct them
 * Amaryllis Vignaud - 23/11/2021
 * Modifs : Add tests for extrude vertex
 * Claire-Marie Alla, Maxime Charzat - 25/11/2021
 * Modifs : Add functions get and set listOfFields for shapefile
 * Amaryllis Vignaud, Axelle Gaige - 2021-11-25
 * Modifs : Add tests for 2,5D vertex
 * Maxime Charzat, Théo Huard - 2021-11-29
*/

#include "gtest/gtest.h"
#include "Shapefile.h"


TEST(shapefile, constructor) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/polygone/polygone.shp");

    //assert
    EXPECT_EQ ("1ershp",  shape.name);
}

TEST(shapefile, load) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/polygone/polygone.shp");

    shape.load();

    //assert
    EXPECT_TRUE (shape.getGdalDataset() != nullptr);
}

TEST(shapefile, calculate_vertex_point_size) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/points/point.shp");

    //act
    shape.load();
    shape.generateVertex();
    std::vector<glm::vec3> vertex = shape.GetVertexPoint();

    //assert
    EXPECT_TRUE (vertex.size() == 1);
}

TEST(shapefile, calculate_vertex_extrude_point_size) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/points/point.shp");

    //act
    shape.load();
    shape.generateVertex(true);
    std::vector<glm::vec3> vertex = shape.GetVertexPoint();
    std::vector<std::vector<glm::vec3>> vertexLine = shape.GetVertexLine();

    //assert
    EXPECT_TRUE (vertex.size() == 0);
    EXPECT_TRUE (vertexLine.size() == 1);
}

TEST(shapefile, calculate_vertex_multi_point_size) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/points/deux_points.shp");

    //act
    shape.load();
    shape.generateVertex();
    std::vector<glm::vec3> vertex = shape.GetVertexPoint();

    //assert
    EXPECT_TRUE (vertex.size() == 2);
}

TEST(shapefile, calculate_vertex_point_coordinates) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/points/deux_points.shp");

    //act
    shape.load();
    shape.generateVertex();
    std::vector<glm::vec3> vertex = shape.GetVertexPoint();

    //assert
    EXPECT_NEAR(841975.3, vertex[0][0], pow(10, -1) );
    EXPECT_NEAR(6520188, vertex[0][1], pow(10, -1) );
}

TEST(shapefile, calculate_vertex_multi_point_coordinates) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/points/deux_points.shp");

    //act
    shape.load();
    shape.generateVertex();
    std::vector<glm::vec3> vertex = shape.GetVertexPoint();

    //assert
    EXPECT_NEAR(841975.4, vertex[0][0], pow(10, -1) );
    EXPECT_NEAR(6520187.9, vertex[0][1], pow(10, -1) );
    EXPECT_NEAR(842072.8, vertex[1][0], pow(10, -1) );
    EXPECT_NEAR(6519963.9, vertex[1][1], pow(10, -1) );
}

TEST(shapefile, calculate_vertex_line) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/linestring/ligne.shp");

    //act
    shape.load();
    shape.generateVertex();
    std::vector<std::vector<glm::vec3>> vertex = shape.GetVertexLine();

    //assert
    EXPECT_TRUE (vertex.size() == 1);
    EXPECT_TRUE (vertex[0].size() == 9);
}

TEST(shapefile, calculate_vertex_extrude_line) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/linestring/ligne.shp");

    //act
    shape.load();
    shape.generateVertex(true);
    std::vector<std::vector<glm::vec3>> vertex = shape.GetVertexLine();
    std::vector<std::vector<glm::vec3>> vertexExtLine = shape.GetVertexExtrudeLine();

    //assert
    EXPECT_TRUE (vertex.size() == 0);
    EXPECT_TRUE (vertexExtLine.size() == 1);
    EXPECT_TRUE (vertexExtLine[0].size() == 18);
}

TEST(shapefile, calculate_vertex_polygon) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/polygone/polygone.shp");

    //act
    shape.load();
    shape.generateVertex();
    std::vector<std::vector<glm::vec3>> vertex = shape.GetVertexPolygon();
    std::vector<std::vector<glm::vec3>> vertexContour = shape.GetVertexContour();

    //assert
    EXPECT_TRUE (vertex.size() == 1);
    EXPECT_TRUE (vertex[0].size() == 6);
    EXPECT_TRUE (vertexContour.size() == 1);
    EXPECT_TRUE (vertexContour[0].size() == 5);
}

TEST(shapefile, calculate_vertex_extrude_polygon) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/polygone/polygone.shp");

    //act
    shape.load();
    shape.generateVertex(true);
    std::vector<std::vector<glm::vec3>> vertex = shape.GetVertexPolygon();
    std::vector<std::vector<glm::vec3>> vertexContour = shape.GetVertexContour();
    std::vector<std::vector<glm::vec3>> vertexExtPol = shape.GetVertexTriangulateExtruded();
    std::vector<std::vector<glm::vec3>> vertexExtContour = shape.GetVertexContourExtruded();

    //assert
    EXPECT_TRUE (vertex.size() == 1);
    EXPECT_TRUE (vertex[0].size() == 6);
    EXPECT_TRUE (vertexContour.size() == 1);
    EXPECT_TRUE (vertexContour[0].size() == 5);
    EXPECT_TRUE (vertexExtPol.size() == 2);
    EXPECT_TRUE (vertexExtPol[0].size() == 6);
    EXPECT_TRUE (vertexExtContour.size() == 7);
    EXPECT_TRUE (vertexExtContour[0].size() == 5);
}

TEST(shapefile, verif_shp) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/polygone/polygone.dbf");

    //assert
    EXPECT_EQ (false, shape.load());
}

TEST(shapefile, verif_dbf_shx) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/testLoad/erreur1.shp");

    //assert
    EXPECT_EQ (false, shape.load());
}

/*
TEST(shapefile, verif_L93) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/testProj/point.shp");
    shape.load();

    //assert
    EXPECT_STREQ ("4171", shape.GetProjection());
}
*/

TEST(shapefile, verif_WGS84) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/testProj/regions-20180101.shp");
    shape.load();

    //assert
    EXPECT_STREQ ("4326", shape.GetProjection());
}

TEST(shapefile, reprojection) {
    //arrange
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/testProj/regions-20180101.shp");

    shape.load();

    // add initial coordinates
    OGRGeometry * geom = shape.getGdalDataset()->GetLayer(0)->GetNextFeature()->GetGeometryRef();
    OGRPoint *poPoint = (OGRPoint *) geom;

    shape.ReprojectShp();

    OGRLayer * layer = shape.getGdalDataset()->GetLayer(0);
    layer->ResetReading();
    OGRGeometry * geom2 = layer->GetNextFeature()->GetGeometryRef();
    OGRPoint *poPoint2 = (OGRPoint *) geom2;

    //assert
    EXPECT_TRUE (poPoint->getX() != poPoint2->getX());
}

TEST(shapefile, verif_setFields){
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/polygone/polygone.shp");
    shape.load();
    shape.setFields();
    EXPECT_EQ("ID", shape.listOfFields[0]);
}

TEST(shapefile, verif_getFields){
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/polygone/polygone.shp");
    shape.load();
    shape.setFields();
    vector<string> fields = shape.getFields();
    EXPECT_EQ("ID", fields[0]);
}

TEST(shapefile, calculateExtentsBarycenter){
    //arrange
    Shapefile shape("shp","../../MonSIG3D/data/Shapefile/polygone/polygone.shp");
    shape.load();
    vector<double> extentsQgis = {839527.8000000000465661,6519140,839542.5999999999767169,6519122.5};

    //act
    shape.calculateExtentsBarycenter();

    //assert
    EXPECT_NEAR(extentsQgis[0],shape.getExtents()[0], pow(10, -1));
    EXPECT_NEAR(extentsQgis[1],shape.getExtents()[1], pow(10, -1));
    EXPECT_NEAR(extentsQgis[2],shape.getExtents()[2], pow(10, -1));
    EXPECT_NEAR(extentsQgis[3],shape.getExtents()[3], pow(10, -1));
}

TEST(shapefile, verif_getContentFeature){
    Shapefile shape("1ershp", "../../MonSIG3D/data/Shapefile/polygone/batiments_arr5.shp");
    shape.load();
    OGRFeature* feature = shape.getGdalDataset()->GetLayer(0)->GetFeature(0);
    vector<string> content = shape.getContentFeature(feature);

    EXPECT_EQ("BATIMENT0000000240872787", content[1]);
}
