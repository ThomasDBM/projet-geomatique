/* test_WMTS.cpp
 * All units tests
 * Roaa Masri
 * First merge : 19/11/2021
*/

#include <iostream>
#include "gtest/gtest.h"
#include "WMTS.h"


TEST(wmts,constructor) {
    //arrange
    std::string wmtsLink ="https://download.data.grandlyon.com/wms/grandlyon?SERVICE=WfS&REQUEST=GetCapabilities&VERSION=1.3.0";
    std::string name = "test_wmts";

    //act
    WMTS WMTStest(name,wmtsLink);
    WMTS WMTStestDeux(name, wmtsLink);

    //assert
    EXPECT_EQ (wmtsLink,WMTStest.path);
    EXPECT_EQ (name,WMTStest.name);
    EXPECT_EQ ("WMTS",WMTStest.type);
    EXPECT_NE (WMTStest.id,WMTStestDeux.id);

}

TEST(wmts, destructor) {
    //arrange


    //act

    //assert
    EXPECT_TRUE (true);
}
