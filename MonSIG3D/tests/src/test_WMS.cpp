/* test_Data.cpp
 * All units tests
 * Mailys Monge
 * First merge : 10/11/2021
 * Implementation Data class test
 * Melodia Mohad
*/

#include <iostream>
#include "gtest/gtest.h"
#include "WMS.h"

using namespace std;


TEST(wms,constructor) {
    //arrange
    string WMS_link ="https://download.data.grandlyon.com/wms/grandlyon?SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.3.0";
    string name = "test_wms";

    //act
    WMS WMStest(name,WMS_link);
    WMS WMStestDeux(name, WMS_link);

    //assert
    EXPECT_EQ (WMS_link,WMStest.path);
    EXPECT_EQ (name,WMStest.name);
    EXPECT_EQ ("WMS",WMStest.type);
    EXPECT_NE (WMStest.id,WMStestDeux.id);
    
}

TEST(wms, destructor) {
    //arrange
    

    //act
    
    //assert
    EXPECT_TRUE (true);
}

TEST(wms,metaHeaderGood) {
    //arrange
    string WMS_link ="https://download.data.grandlyon.com/wms/grandlyon?SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.3.0";
    string name = "test-wms";
    WMS WMStest(name,WMS_link);

    //act & assert

    EXPECT_TRUE (WMStest.getMetaHeader());



}
TEST(wms,metaHeaderBad) {
    //arrange
    string WMS_link ="https://download.data.grandlyon.com/wms/grand?SERVICE=WMS&REQUEST=GetCapabilities&VERSION=1.3.0";
    string name = "test-wms";
    WMS WMStest(name,WMS_link);

    //act & assert
    EXPECT_THROW(WMStest.getMetaHeader(),invalid_argument);

}
