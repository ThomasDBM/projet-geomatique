/**
 * @file test_Data.cpp
 * @author Melodia Mohad
 * @brief
 * @version 0.1
 * @date 2021-11-17
 *
 * @copyright Copyright (c) 2021
 *
 * Modifs : change the geotiff file used in tests
 * Maxime Charzat - 2021-12-01
 */

#include <iostream>
#include "gtest/gtest.h"
#include "Geotiff.h"

using namespace std;

TEST(geotiff, constructor){
    //arrange
    string path = "../../MonSIG3D/data/GeoTIFF/Lyon_arr5_petit/ortho_arr5_resize.tif";
    string name = "Fond de carte";

    //act
    Geotiff geotiff(name, path, "../../MonSIG3D/data/GeoTIFF/Lyon_arr5/mnt_arr5.tif");

    //assert
    EXPECT_EQ(path,geotiff.getPath());
    EXPECT_EQ(name,geotiff.getName());
}


TEST(geotiff, calculateExtentsBarycenter){
    //arrange
    Geotiff tif("tif","../../MonSIG3D/data/GeoTIFF/Lyon_arr5_petit/ortho_arr5_emprise_moyenne.tif","../../MonSIG3D/data/GeoTIFF/Lyon_arr5/mnt_arr5.tif");

    vector<double> extentsQgis = {839602.0000000000000000,6519340.0000000000000000, 840976.0000000000000000,6518171.0000000000000000};

    //act
    tif.calculateExtentsBarycenter();

    //assert
    EXPECT_NEAR(extentsQgis[0],tif.getExtents()[0], pow(10, -1));
    EXPECT_NEAR(extentsQgis[1],tif.getExtents()[1], pow(10, -1));
    EXPECT_NEAR(extentsQgis[2],tif.getExtents()[2], pow(10, -1));
    EXPECT_NEAR(extentsQgis[3],tif.getExtents()[3], pow(10, -1));
}
