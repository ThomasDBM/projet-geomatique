# Convention de commit

Commiter régulière le code.

Les commit s'écrivent de la manière suivante :   
`<type>(<portée>) : <brève-description>`(obligatoire)   
`<détail>`(option)


## Type
Les type de commit sont : 
- build : changement qui vont affecter le système de build ou des dépendance externe (npm,make...)
- ci : changement dans les fichiers d'intégration
- feat : ajout d'une nouvelle fonctionnalité
- fix : correction d'un bug
- perf : amélioration des performances
- refractor : modification qui n'apporte ni nouvelle fonctionnalité ni d'amélioration de performances
- style : changement qui n'apporte aucune altération fonctionnelle ou sémantique (indentation, mise forme, ajout d'espace, ...)
- docs : rédaction ou mise à jour de documentation
- test : ajout d'une modification de tests
  
## Portée
La portée permet d'indiquer sur quel composant la modification a eu lieu. 
Exemple : view, controlleur, model, ...

## Description
La description est brève et succinte.

## Exemple de commit :
- `feat(lang) : add new language`
- `bug(view) : fix shift map`
- `doc(controller) : update documentation for function treatment`

## Détail
Il est possible d'ajouter des détails au commit si nécessaire mais il faut rester bref dans les explications.

