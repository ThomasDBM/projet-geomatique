# Convention de merge

## Merge request

### Merge perso -> dev 

- Eléments nécessaires au merge:
    1. Modification en lien avec une user story
    2. Documentation à jour et qui suit la convention
    3. Test unitaire défini
    4. Phase de testing validée
- Demande de merge request :
    1. Mettre un titre en rapport avec la user story
    2. Ecrire une description des modifications effectuées
    3. Assigner l'étiquette à un reviewer
    4. Le reviewer doit verifier:
        * les conditons précédentes sont valides
        * la cohérence entre la user story et le ticket de merge request
        * les tests pipeline sont valides
Si tout est bon le reviewer valide la merge request et la renvoie au développeur qui lance le merge.

### Merge dev -> master

Ce merge est effectué uniquement par le product owner. Cette tâche est réalisée uniquement lors de **la fin d'un sprint** ou **à la demande du client**.

- Eléments nécessaires au merge:
   1. Le product owner prévient l'équipe de dev qu'une merge request dev -> master va avoir lieu : **La branche dev est bloquée**
   2. Iels font un testing de la branche dev 
       1. Si la phase de testing ne passe pas, iel crée une branche hotfix et une user story associée aux bugs avant de poursuivre
   
Si aucun bug ne persiste sur la branch dev, iel peut lancer une merge request.

- Demande de merge request :
    1. Titre : Nom de l'appli et numéro de version
    2. Description des nouvelles features et des modifications apportées
    3. Assigne un reviewer
    4. Le reviewer vérifie :
       - La présence du titre et d'une description et la bonne compréhension du titre
       - Check la pipeline
    5. Le reviewer valide la demande
    6. Le product owner effectue le merge.

Une fois le merge effectué, iel pense à préciser aux dev qu'ils peuvent reprendre les merges sur dev.
