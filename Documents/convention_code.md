# Convention de code

## Règles générales

- La documentaion et le code sera écrit en anglais. De même pour les interactions de git
- Une ligne de code ou de commentaire ne doit pas dépasser 100 carractères.
- Une ligne de documentation ne doit pas dépasser 100 caractères.
- Ne pas utiliser de tabulations pour indenter mais préférer des espaces. 1 tabulations = 4 espaces.
- Le code **doit** être indenté pour le rendre visible. Cf norme SUN.
- Les #include doivent être enoncés dans l'ordre **alphabétique**
- Les commentaires doivent suivre la documentation de l'outil Doxygen cf [documentation Doxygen](https://www.doxygen.nl/manual/docblocks.html)
- Pas de définition de fonctions avant le main ni de classe.
- Les passages par références ne doivent être utilisés que lorsqu'une valeur de sortie est transmise par effet de bord.


## Fichier source

Nommage fichier source :
  1. Il ne doit exister qu'un seul fichier main
  2. Les noms des fichiers doivent correspondre au nom de la classe associé et commencer par une majuscule

Contenu des entêtes:
  1. Nom du programme (ie, le nom du fichier source)
  2. Une courte description (4-5 lignes max)
  3. Les auteurs
  4. La date du premier merge pour le fichier
  5. Une description plus longue qui contient:
       - Les entrées du programme et leurs préconditions
       - Les sorties du programme et leurs préconditions
  6. La date de fin de chaque modifications avec les auteurs associées
  7. une description des modificatons pour chaque version

Un fichier source ne doit pas dépasser **2000 lignes** code et commentaire compris.

## Méthodes et fonctions

Nommage des méthodes et fonctions :
  1. exemple: **maFonctionFaitUnTruc()**
  2. Cas des fonctions retournant un booléen :
       - Elle doivent débuter par un **adjectif** ou une **expression propositionnelle** ex: vide(), estVide().
  3. Cas des fonctions ayant un effet de bord ou modifiant les propriétés d'un objet:
       - Elle doivent débuter par un **verbe à l'infinitif** ex: vider(), calculerCheminLePlusCourt()

Documentation des méthodes et fonctions :
  1. Une entête avec :
       - Description du traitement effectué
       - Paramètres d'entrés ex: nomVariablesEntrée : type
       - Paramètres de sortie ex: nomVariablesSortie : type
       - Les paramètes par valeurs sont déclarés avant les paramètres par référence (pointeur)
  2. Peut comprendre des commentaire dans le corps si la partie est complexe


Une méthode/fonction ne doit pas faire plus de **30 lignes**.

## Nommages des variables

1. Les indices d'itérations :
      - Peuvent être des lettres simples (ex: matrice (i,j))
      - Dans le cas ou les indices dénotent un concept particulier, il est préférable d'utiliser un nom significatif ex: matrice 3d (pays, province, comte)
2. Un nom doit avoir un seul sens dans un programme et être explicite ex: on ne dit pas juste nombre mais **nombreDeLivre**
3. Un concept doit avoir un et un seul nom. On choisit entre **toAdd()** et **toLoad()** pour l'action qui charge un fichier utilisateur.
4. Un nom de constante doit être écrit en majuscule ex: NB_MAX_ITERATION
5. La séparation des mots se fait par un changement de cast de la première lettre du mot sauf pour le cas des constantes. La séparation des mots pour les constantes se fait avec "_". ex: **nombreDelivre**, **NOMBRE_MAX_DE_LIVRE**

## 
